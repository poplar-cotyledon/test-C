package com.yzy.twohundred;

import java.util.*;

public class FileCachingSystem_200_18 {
    static class File {
        String fileName;
        int fileSize;
        int visitCount;

        public File(String fileName, int fileSize, int visitCount) {
            this.fileName = fileName;
            this.fileSize = fileSize;
            this.visitCount = visitCount;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int maxCaching = Integer.parseInt(sc.nextLine());
        int remainCaching = maxCaching;
        int commandNum = Integer.parseInt(sc.nextLine());

        Map<String, File> filesMap = new HashMap<>();
        //优先队列：按照访问次数从小到大排列，相等则最近插入的放在靠后位置 —— 从队头删
        PriorityQueue<String> visitQueue = new PriorityQueue<>((s1, s2) -> {
            if (filesMap.get(s1).visitCount != filesMap.get(s2).visitCount) {
                return filesMap.get(s1).visitCount - filesMap.get(s2).visitCount;
            }
            return 0;
        });

        for (int i = 0; i < commandNum; i++) {
            String[] command = sc.nextLine().split(" ");
            String ops = command[0];
            String fileName = command[1];
            if (ops.equals("put")) {
                //存储文件
                int fileSize = Integer.parseInt(command[2]);
                if (fileSize > maxCaching) {
                    continue;
                }
                //如果文件缓存中已有该文件名，则跳过该命令
                if (!visitQueue.contains(fileName)) {
                    //不够存时：
                    while (fileSize > remainCaching) {
                        String poll_fileName = visitQueue.poll();
                        remainCaching += filesMap.get(poll_fileName).fileSize;
                    }
                    filesMap.put(fileName, new File(fileName, fileSize, 0));
                    visitQueue.add(fileName);
                    remainCaching -= fileSize;
                }
            } else {
                //读取文件
                //如果文件缓存中不含该文件名，则跳过该命令
                if (filesMap.containsKey(fileName)) {
                    visitQueue.remove(fileName);
                    File file = filesMap.get(fileName);
                    file.visitCount++;
                    visitQueue.add(fileName);
                }
            }
        }

        if(remainCaching == maxCaching){
            System.out.println("NONE");
        }else {
            StringJoiner sj = new StringJoiner(",");
            visitQueue.stream().sorted(String::compareTo).forEach(sj::add);
            System.out.println(sj);
        }
    }
}