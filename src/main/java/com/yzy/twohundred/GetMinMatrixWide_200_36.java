package com.yzy.twohundred;

import java.util.Scanner;

public class GetMinMatrixWide_200_36 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt();
        int col = sc.nextInt();
        int[][] matrix = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }
        int len = sc.nextInt();
        int[] arr = new int[1000];//记录的是目标数组中num元素的个数
        for (int i = 0; i < len; i++) {
            int num = sc.nextInt();
            arr[num]++;
        }

        int total = len;//未完成匹配的元素的个数
        int minWide = Integer.MAX_VALUE;//记录最小子矩阵的宽度
        int l = 0;//当前子矩阵的左边界（列号）
        int r = 0;//当前子矩阵的右边界（列号）
        while (r < col) {
            //1.将第r列所有元素纳入子矩阵
            for (int i = 0; i < row; i++) {
                int num = matrix[i][r];
                if (arr[num]-- > 0) {
                    total--;
                }
            }
            //2.当total==0时，
            while (total == 0) {
                //① 更新矩阵宽度
                minWide = Math.min(minWide, r - l + 1);
                //② l右移，尝试更小矩阵
                for (int i = 0; i < row; i++) {
                    int num = matrix[i][l];
                    if (arr[num]++ >= 0) {
                        total++;
                    }
                }
                l++;
            }
            //3.当total>0时，r右移
            r++;
        }
        System.out.println(minWide == Integer.MAX_VALUE ? -1 : minWide);
    }
}
