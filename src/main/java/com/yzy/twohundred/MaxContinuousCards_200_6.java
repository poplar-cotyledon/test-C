package com.yzy.twohundred;

import java.util.Arrays;
import java.util.Scanner;

public class MaxContinuousCards_200_6 {
    static class Card {
        int num;
        char color;

        public Card(int num, String color) {
            this.num = num;
            this.color = color.charAt(0);
        }
    }

    static Card[] cards;
    static boolean[] used;
    static int result = 0;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] nums = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        String[] colors = sc.nextLine().split(" ");

        int n = nums.length;
        cards = new Card[n];
        used = new boolean[n];
        for (int i = 0; i < n; i++) {
            cards[i] = new Card(nums[i], colors[i]);
        }
        dfs(null, 0);
        System.out.println(result);
    }

    public static void dfs(Card last, int count) {
        result = Math.max(result, count);
        for (int i = 0; i < cards.length; i++) {
            //去重
            if (used[i]) {
                continue;
            }
            Card cur = cards[i];
            if (last != null && last.num != cur.num && last.color != cur.color) {
                continue;
            }
            used[i] = true;
            dfs(cur, count + 1);
            used[i] = false;
        }
    }
}
