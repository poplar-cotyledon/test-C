package com.yzy.twohundred;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

public class GreedySinger_200_38 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int totalTime = sc.nextInt();
        int cities = sc.nextInt();
        int costTime = 0;
        for (int i = 0; i < cities + 1; i++) {
            costTime += sc.nextInt();
        }
        int[][] incomes = new int[cities][2];
        for (int i = 0; i < cities; i++) {
            incomes[i][0] = sc.nextInt();
            incomes[i][1] = sc.nextInt();
        }

        int remainTime = totalTime - costTime;
        if (remainTime <= 0) {
            System.out.println(0);
            return;
        }
        //优先队列（小顶堆）记录赚到的钱, 即堆顶是某天赚的最少的钱
        PriorityQueue<Integer> pq = new PriorityQueue<>(Comparator.comparingInt(a -> a));
        for (int[] income : incomes) {
            //第一天卖唱可以赚a，后续每天的收入会减少d
            int a = income[0];
            int d = income[1];
            //只要在当前城市还有钱赚，那么就继续待
            while (a > 0) {
                if (pq.size() >= remainTime) {
                    if (pq.peek() < a) {
                        pq.poll();
                    } else {
                        break;
                    }
                }
                pq.add(a);
                a -= d;
            }
        }
        System.out.println(pq.stream().reduce(Integer::sum).orElse(0));
    }
}
