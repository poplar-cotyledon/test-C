package com.yzy.twohundred;

import java.util.*;

public class Archaeologist_200_48 {
    static int n;
    static String[] words;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        n = Integer.parseInt(sc.nextLine());
        words = sc.nextLine().split(" ");
        Arrays.sort(words);

        boolean[] used = new boolean[n];
        LinkedList<String> path = new LinkedList<>();
        Set<String> result = new HashSet<>();
        dfs(used, path, result);

        //输出石碑文字的组合（按照升序排列）
        result.stream().sorted(String::compareTo).forEach(System.out::println);
    }

    public static void dfs(boolean[] used, LinkedList<String> path, Set<String> result) {
        //结束条件：
        if (path.size() == n) {
            StringBuilder sb = new StringBuilder();
            path.forEach(sb::append);
            result.add(sb.toString());
            return;
        }
        //循环条件：
        for (int i = 0; i < n; i++) {
            //去重
            if (used[i]) {
                continue;
            }
            //树层去重
            if (i > 0 && words[i].equals(words[i - 1]) && !used[i - 1]) {
                continue;
            }
            path.add(words[i]);
            used[i]=true;
            dfs(used,path,result);
            used[i]=false;
            path.removeLast();
        }
    }
}
