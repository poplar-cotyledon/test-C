package com.yzy.twohundred;

import java.util.*;

public class Woodcutter_200_31 {
    static class Wood {
        int profit;//记录木材的最大收益
        List<Integer> slices = new ArrayList<>();//记录木材最大收益时对应的切割状态

        public Wood(int profit) {
            this.profit = profit;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();

        Wood[] dp = new Wood[x + 1];
        //初始时，将木材不切割的收益作为最大收益
        for (int i = 0; i <= x; i++) {
            dp[i] = new Wood(i);
            dp[i].slices.add(i);
        }

        //长度为0~4的木材要么不可切割，要么切割后结果还不如不切割
        //因此，从长度为5的木材开始尝试切割
        for (int i = 5; i <= x; i++) {
            for (int j = 1; j <= i / 2; j++) {
                int profit = dp[j].profit * dp[i - j].profit;
                //如果该切割方案的收益更大，或者收益和之前切割方案的收益相同，但是切割数更少，则使用当前切割方案
                if (profit > dp[i].profit || ((profit == dp[i].profit) && dp[i].slices.size() > dp[j].slices.size() + dp[i - j].slices.size())) {
                    dp[i].profit = profit;
                    dp[i].slices.clear();
                    dp[i].slices.addAll(dp[j].slices);
                    dp[i].slices.addAll(dp[i - j].slices);
                }
            }
        }

        //dp[x].slices记录的是：长度x的木材对应的最大收益的切割方案
        //按题目输出描述要求进行升序
        dp[x].slices.sort(Comparator.comparingInt(o -> o));

        //打印切割方案
        StringJoiner sj = new StringJoiner(" ");
        for (int slice : dp[x].slices) {
            sj.add(slice + "");
        }
        System.out.println(sj);
    }
}