package com.yzy.twohundred;

import java.util.Arrays;
import java.util.Scanner;

public class StringConcatenation_200_29 {
    static char[] chars;
    static int n;
    static boolean[] used;
    static int result;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] split = sc.nextLine().split(" ");
        chars = split[0].toCharArray();
        n = Integer.parseInt(split[1]);
        if (chars.length < n) {
            System.out.println(0);
            return;
        }
        for (char c : chars) {
            if (c < 'a' || c > 'z') {
                System.out.println(0);
                return;
            }
        }
        Arrays.sort(chars);
        used = new boolean[chars.length];
        System.out.println(dfs(-1, 0));
    }

    /**
     * 全排列求解
     *
     * @param pre   排列最后一个字符在chars中的位置
     * @param level 排列的长度
     * @return count
     */
    public static int dfs(int pre, int level) {
        //结束条件：
        if (level == n) {
            return ++result;
        }
        //进入循环：
        for (int i = 0; i < chars.length; i++) {
            //每个字符只能用一次
            if (used[i]) {
                continue;
            }
            //相同的字符不能相邻
            if (pre >= 0 && chars[pre] == chars[i]) {
                continue;
            }
            //树层去重(去除重复排列)
            if (i > 0 && chars[i] == chars[i - 1] && !used[i - 1]) {
                continue;
            }
            used[i] = true;
            result = dfs(i, level + 1);
            used[i] = false;
        }
        return result;
    }
}
