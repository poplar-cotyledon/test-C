package com.yzy.twohundred;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AnalogDataEncodeAndDecode_200_60 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int command = Integer.parseInt(sc.nextLine());
        String input = sc.nextLine();
        switch (command) {
            //编码
            case 1:
                try {
                    System.out.println(encode(input));
                } catch (Exception e) {
                    System.out.println("ENCODE_ERROR");
                }
                break;
            //解码
            case 2:
                try {
                    System.out.println(decode(input));
                } catch (Exception e) {
                    System.out.println("DECODE_ERROR");
                }
                break;
        }
    }

    //编码
    static Map<String, String> encodeMap = new HashMap<>();
    //  [1,String,I am Mary],[2,Integer,23],[3,Long,1000000],[4,Compose,[1,String,I am Kitty],[2,Integer,44]]
    private static String encode(String input) {
        if (input.length() > 1000) {
            return "ENCODE_ERROR";
        }
        encodeMap.put("Integer", "0");
        encodeMap.put("String", "1");
        encodeMap.put("Compose", "2");

        //1.将数据与数据之间的逗号分隔去除
        //  [1,String,I am Mary][2,Integer,23][3,Long,1000000][4,Compose,[1,String,I am Kitty][2,Integer,44]]
        input = input.replaceAll("],\\[", "][");
        //2.检查整体编码格式
        if (!check_encode_format(input)) {
            return "ENCODE_ERROR";
        }
        //3.由于[]是正则表达式的元字符，因此将所有[]替换为<>
        //  <1,String,I am Mary><2,Integer,23><3,Long,1000000><4,Compose,<1,String,I am Kitty><2,Integer,44>>
        input = input.replaceAll("\\[", "<").replaceAll("]", ">");
        //4.用正则表达式匹配出待编码字符串中<位置,类型,值>
        //  <1,String,I am Mary><2,Integer,23><3,Long,1000000><1,String,I am Kitty><2,Integer,44>
        Pattern p = Pattern.compile("<([^<>]+)>");
        while (true) {
            Matcher m = p.matcher(input);
            if (!m.find()) {
                break;
            }
            //m.group(0)是正则匹配到的完整子串 —— <位置,类型,值> —— <1,String,I am Mary>
            //m.group(1)用于获取是第一个正则捕获组，即去除<>之后的子串 —— 位置,类型,值 —— 1,String,I am Mary
            String[] tmp = m.group(1).split(",");
            String pos = tmp[0];
            String type = tmp[1];
            String data = tmp[2];

            //5.检查数据区格式
            //  如果合法，则匹配到的内容替换为编码后的内容；如果非法，则用空替换，相当于删除该格式错误的数据区
            String encodeStr = "";
            if (check_encode(pos, type, data)) {
                encodeStr = pos + "#" + encodeMap.get(type) + "#" + data.length() + "#" + data;
            }
            input = input.replace(m.group(0), encodeStr);
        }
        return input;
    }

    //检查整体编码格式：合法返回true，非法返回false
    //  [][][][[][]]
    private static boolean check_encode_format(String input) {
        Stack<Character> stack = new Stack<>();
        char[] chars = input.toCharArray();
        //1.整体编码格式必须以[开头，以]结尾
        if (input.charAt(0) != '[' || input.charAt(input.length() - 1) != ']') {
            return false;
        }
        //2.括号[]必须成对匹配
        for (char c : chars) {
            if (c == '[') {
                stack.push(c);
            } else if (c == ']') {
                if (stack.isEmpty() || stack.peek() != '[') {
                    return false;
                }
                stack.pop();
            }
        }
        return stack.isEmpty();
    }

    static Pattern num_RegExp = Pattern.compile("^\\d+$");
    static Pattern str_RegExp = Pattern.compile("^[0-9a-zA-Z\\s]+$");

    //检查数据区格式：合法返回true，非法返回false
    private static boolean check_encode(String pos, String type, String data) {
        if (!num_RegExp.matcher(pos).find() || !encodeMap.containsKey(type)) {
            return false;
        }
        if ("Integer".equals(type)) {
            return num_RegExp.matcher(data).find();
        } else if ("String".equals(type)) {
            return str_RegExp.matcher(data).find();
        } else {
            //遇到Compose说明它的data区已经全部编码完毕
            return true;
        }
    }

    //解码
    static Map<String, String> decodeMap = new HashMap<>();
    //  1#1#9#I am Mary2#0#2#233#0#3#8794#2#25#1#1#10#I am Kitty2#0#2#44
    private static String decode(String input) {
        if (input.length() > 1000) {
            return "DECODE_ERROR";
        }
        decodeMap.put("0", "Integer");
        decodeMap.put("1", "String");
        decodeMap.put("2", "Compose");

        //1.直接将待解码字符串按照'#'分割, 并加入到队列中, 方便头部出队
        LinkedList<String> queue = new LinkedList<>();
        //  1，1，9，I am Mary2，0，2，233，0，3，8794，2，25，1，1，10，I am Kitty2，0，2，44
        Collections.addAll(queue, input.split("#"));
        //2.遍历队列解码
        //  res记录解码后的内容
        StringJoiner res = new StringJoiner(",");
        while (!queue.isEmpty()) {
            //①如果格式合法，则队列前3个分别是——位置、类型、值的长度
            String pos = queue.removeFirst();// 1
            String type = queue.removeFirst();// 1
            int len = Integer.parseInt(queue.removeFirst());// 9
            //②剩余部分重新以#拼接
            String remain = String.join("#", queue);// I am Mary2#0#2#233#0#3#8794#2#25#1#1#10#I am Kitty2#0#2#44
            queue.clear();
            //③remain字符串的 [0,len) 部分作为当前数据区的data信息
            String data = remain.substring(0, len);// I am Mary
            if (remain.length() > len) {
                // remain字符串的 [len,end) 部分是其他数据区，按照#分割后重新入队
                Collections.addAll(queue, remain.substring(len).split("#"));// 2，0，2，233，0，3，8794，2，25，1，1，10，I am Kitty2，0，2，44
            }
            //④嵌套结构的解码使用递归解决
            if ("2".equals(type)) {
                data = decode(data);
            }
            //⑤pos, type, data都合法则进行解码，否则跳过解码
            if (check_decode(pos, type, data)) {
                res.add("[" + pos + "," + decodeMap.get(type) + "," + data + "]");
            }
        }
        return res.toString();
    }

    //检查数据区格式：合法返回true，非法返回false
    private static boolean check_decode(String pos, String type, String data) {
        if (!num_RegExp.matcher(pos).find() || !decodeMap.containsKey(type)) {
            return false;
        }
        if ("0".equals(type)) {
            return num_RegExp.matcher(data).find();
        } else if ("1".equals(type)) {
            return str_RegExp.matcher(data).find();
        } else {
            return true;
        }
    }
}
