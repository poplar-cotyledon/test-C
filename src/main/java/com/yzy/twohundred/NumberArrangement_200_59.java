package com.yzy.twohundred;

import java.util.*;

public class NumberArrangement_200_59 {
    static int[] nums;
    static boolean[] used;
    static List<Integer> result = new ArrayList<>();
    static Map<Integer, Integer> map = new HashMap<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        nums = Arrays.stream(sc.nextLine().split(",")).mapToInt(Integer::parseInt).toArray();

        Set<Integer> set = new HashSet<>();
        int n = 0;
        for (int num : nums) {
            if (num < 1 || num > 9) {
                //输入的数字不在范围内
                System.out.println(-1);
                return;
            } else {
                set.add(num);
                n = Math.max(n, num);
            }
        }
        //输入的数字有重复
        if (set.size() != nums.length) {
            System.out.println(-1);
            return;
        }
        //屏幕不能同时给出 2 和 5
        if (set.contains(2) && set.contains(5)) {
            System.out.println(-1);
            return;
        }
        //屏幕不能同时给出 6 和 9
        if (set.contains(6) && set.contains(9)) {
            System.out.println(-1);
            return;
        }

        used = new boolean[nums.length];
        map.put(2, 5);
        map.put(5, 2);
        map.put(6, 9);
        map.put(9, 6);

        String path = "";
        dfs(path);

        result.sort(Comparator.comparingInt(a -> a));
        n = Math.min(n, result.size());
        System.out.println(result.get(n - 1));
    }

    private static void dfs(String path) {
        if (!path.isEmpty()) {
            result.add(Integer.parseInt(path));
        }
        //结束条件：
        if (path.length() == nums.length) {
            return;
        }
        //进入循环：
        for (int i = 0; i < nums.length; i++) {
            //去重
            if (used[i]) {
                continue;
            }
            used[i] = true;
            dfs(path + nums[i]);
            // 2 可以当作 5 来使用，5 也可以当作 2 来使用进行数字拼接
            // 6 可以当作 9 来使用，9 也可以当作 6 来使用进行数字拼接
            if (map.containsKey(nums[i])) {
                dfs(path + map.get(nums[i]));
            }
            used[i] = false;
        }
    }
}
