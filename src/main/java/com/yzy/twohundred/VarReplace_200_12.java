package com.yzy.twohundred;

import java.util.Arrays;
import java.util.Scanner;

public class VarReplace_200_12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        String[] inputArr = s.split(",");

        for (int i = 0; i < inputArr.length; i++) {
            if (check(i, inputArr) == null) {
                System.out.println("-1");
                return;
            }
        }
        System.out.println(String.join(",", Arrays.asList(inputArr)));
    }

    private static String check(int index, String[] inputArr) {
        String input = inputArr[index];
        int a = input.indexOf("<");
        int b = input.indexOf(">");
        if (a == -1 && b == -1) {
            //单元格无引用变量
            return input;
        } else if (a > -1 && b - a == 2) {
            //单元格含引用变量，且正确引用
            char c = input.charAt(a + 1);
            String temp = check(c - 'A', inputArr);
            if (temp == null) {
                return null;
            }
            String result = input.replace("<" + c + ">", temp);
            inputArr[index] = result;
            return result;
        } else {
            //错误引用
            return null;
        }
    }
}