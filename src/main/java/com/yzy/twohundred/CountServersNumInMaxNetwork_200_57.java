package com.yzy.twohundred;

import java.util.*;

public class CountServersNumInMaxNetwork_200_57 {
    static int row;
    static int col;
    static int[][] matrix;
    //上、下、左、右
    static int[][] offsets = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        row = sc.nextInt();
        col = sc.nextInt();
        matrix = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }

        int result = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (matrix[i][j] == 1) {
                    result = Math.max(result, bfs(i, j));
                }
            }
        }
        System.out.println(result);
    }

    public static int bfs(int i, int j) {
        int count = 1;
        matrix[i][j] = 0;

        LinkedList<int[]> queue = new LinkedList<>();
        queue.add(new int[]{i, j});
        while (queue.size() > 0) {
            int[] loc = queue.removeFirst();
            int x = loc[0];
            int y = loc[1];
            for (int[] offset : offsets) {
                int newX = x + offset[0];
                int newY = y + offset[1];
                if (newX >= 0 && newX < row && newY >= 0 && newY < col && matrix[newX][newY] == 1) {
                    count++;
                    matrix[newX][newY] = 0;
                    queue.add(new int[] {newX, newY});
                }
            }
        }
        return count;
    }
}
