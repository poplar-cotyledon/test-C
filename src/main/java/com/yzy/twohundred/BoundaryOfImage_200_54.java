package com.yzy.twohundred;

import java.util.*;

public class BoundaryOfImage_200_54 {
    //并查集实现：
    //存储结构——树的双亲表示法，用一个数组即可表示集合关系
    static class UFSet {
        int[] father;
        int count;

        //初始化
        public UFSet(int n) {
            //数组值为-1的元素，即为根结点
            this.father = new int[n];
            this.count = n;
            for (int i = 0; i < n; i++) {
                father[i] = -1;
            }
        }

        //并 +优化(让树不长高)
        public void union(int x, int y) {
            int rootX = this.find(x);//找到x的根结点rootX
            int rootY = this.find(y);//找到y的根结点rootY
            //若根结点相同，则返回
            if (rootX == rootY) {
                return;
            }
            //father[]<0,越小，说明包含结点越多
            if (father[rootX] >= father[rootY]) {
                //以rootY为根
                this.father[rootY] += this.father[rootX];//累加结点总数
                this.father[rootX] = rootY;//小树合并到大树
            } else {
                //以rootX为根
                this.father[rootX] += this.father[rootY];//累加结点总数
                this.father[rootY] = rootX;//小树合并到大树
            }
            this.count--;
        }

        //查 +优化(压缩路径)
        public int find(int x) {
            int root = x;
            //循环寻找x所属的最终根结点
            while (father[root] > 0) {
                root = father[root];
            }
            //将循环找根结点的路径上，所有结点的父结点置为根结点
            while (father[x] != root && father[x] > 0) {
                int temp = father[x];
                father[x] = root;//x直接挂在根结点下
                x = temp;
            }
            return root;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int hang = sc.nextInt();
        int lie = sc.nextInt();
        int[][] map = new int[hang][lie];
        for (int i = 0; i < hang; i++) {
            for (int j = 0; j < lie; j++) {
                map[i][j] = sc.nextInt();
            }
        }
        System.out.println(getResult(map, hang, lie));
    }

    //将题目转化为：求并查集有几个连通分量
    private static int getResult(int[][] map, int hang, int lie) {
        //上、下、左、右、左上、左下、右上、右下的横坐标、纵坐标偏移量
        int[][] offset = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}, {-1, -1}, {-1, 1}, {1, -1}, {1, 1}};
        //记录所有边界位置
        Set<Integer> boundary = new HashSet<>();
        for (int x = 0; x < hang; x++) {
            for (int y = 0; y < lie; y++) {
                // 如果当前点是像素5
                if (map[x][y] == 5) {
                    // 遍历像素5的相邻位置
                    for (int[] o : offset) {
                        int newX = x + o[0];
                        int newY = y + o[1];
                        // 如果该位置不越界，且为像素1，则是边界
                        if (newX >= 0 && newX < hang &&
                                newY >= 0 && newY < lie &&
                                map[newX][newY] == 1) {
                            boundary.add(newX * lie + newY);//存入边界的一维坐标
                        }
                    }
                }
            }
        }
        //set转list
        List<Integer> boundaryList = new ArrayList<>(boundary);
        int k = boundaryList.size();
        //使用并查集，对所有边界位置进行合并
        UFSet ufs = new UFSet(k);
        for (int i = 0; i < k; i++) {
            int x1 = boundaryList.get(i) / lie;
            int y1 = boundaryList.get(i) % lie;

            for (int j = i + 1; j < k; j++) {
                int x2 = boundaryList.get(j) / lie;
                int y2 = boundaryList.get(j) % lie;

                // 如果两个边界像素1的位置 横向、纵向距离均小于1，则相邻，可以进行合并
                if (Math.abs(x1 - x2) <= 1 && Math.abs(y1 - y2) <= 1) {
                    ufs.union(i, j);
                }
            }
        }
        return ufs.count;
    }
}