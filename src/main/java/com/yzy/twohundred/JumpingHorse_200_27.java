package com.yzy.twohundred;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Set;

public class JumpingHorse_200_27 {
    //棋盘行数
    static int row;
    //棋盘列数
    static int col;
    //棋盘矩阵
    static char[][] matrix;
    //最小步数和矩阵，path[i][j]记录各个马走到棋盘(i,j)位置的最小步数之和，矩阵每个元素初始值为0
    static int[][] path;
    //记录所有马都可达的公共位置坐标，初始时该集合记录棋盘所有位置,即认为所有马可以跳到所有位置，所有位置都是公共位置
    static Set<Integer> reach;
    //马走日的偏移量
    static int[][] offsets = {{1, 2}, {1, -2}, {2, 1}, {2, -1}, {-1, 2}, {-1, -2}, {-2, 1}, {-2, -1}};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        row = sc.nextInt();
        col = sc.nextInt();
        matrix = new char[row][col];
        path = new int[row][col];
        reach = new HashSet<>();
        for (int i = 0; i < row; i++) {
            matrix[i] = sc.next().toCharArray();
            for (int j = 0; j < col; j++) {
                reach.add(i * col + j);
            }
        }

        //遍历棋盘，每匹马走日
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                //如果棋盘(i,j)位置是马
                if (matrix[i][j] != '.') {
                    //马的等级
                    int k = matrix[i][j] - '0';
                    //对该马进行BFS走日
                    bfs(i, j, k);
                }
            }
        }
        //如果所有马走完，发现没有公共可达位置
        if (reach.size() == 0) {
            System.out.println(-1);
        }
        //记录所有马都可达位置的最小步数和
        int minStep = Integer.MAX_VALUE;
        for (Integer r : reach) {
            int x = r / col;
            int y = r % col;
            minStep = Math.min(minStep, path[x][y]);
        }
        System.out.println(minStep);
    }

    //BFS
    private static void bfs(int sx, int sy, int k) {
        //queue记录该马在第几层的所有位置信息
        LinkedList<int[]> queue = new LinkedList<>();
        queue.add(new int[]{sx, sy, 0});//(x,y)为马所在初始位置，马到达初始位置需要0步
        //记录该马可以访问(x,y)位置
        Set<Integer> visit = new HashSet<>();
        visit.add(sx * col + sy);//二维坐标一维化
        //BFS
        while (queue.size() > 0 && k > 0) {
            //newQueue记录该马走step（0≤step≤k）步的所有位置信息（即BFS按层遍历的层）
            LinkedList<int[]> newQueue = new LinkedList<>();
            for (int[] q : queue) {
                int x = q[0];
                int y = q[1];
                int step = q[2];
                for (int[] offset : offsets) {
                    //马走日到达的新位置
                    int newX = x + offset[0];
                    int newY = y + offset[1];
                    int newLoc = newX * col + newY;
                    //如果新位置越界，或已经跳过(新位置在vis集合中)，则continue
                    if (newX < 0 || newX >= row || newY < 0 || newY >= col || visit.contains(newLoc)) {
                        continue;
                    }
                    //否则，加入newQueue，更新path，且加入visit
                    newQueue.add(new int[]{newX, newY, step + 1});
                    path[newX][newY] += step + 1;
                    visit.add(newLoc);
                }
            }
            queue = newQueue;
            k--;
        }
        //BFS完后，将公共可达位置reach和当前马可达位置取交集，交集部分就是新的公共可达位置
        reach.retainAll(visit);
    }
}
