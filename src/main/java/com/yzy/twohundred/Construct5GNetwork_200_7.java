package com.yzy.twohundred;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Construct5GNetwork_200_7 {
    // 边
    static class Edge {
        int from; // 边起点
        int to; // 边终点
        int weight; // 边权重

        public Edge(int from, int to, int weight) {
            this.from = from;
            this.to = to;
            this.weight = weight;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(); // 基站数量（节点数）
        int m = sc.nextInt(); // 基站对数量（边数）

        Edge[] edges = new Edge[m];
        for (int i = 0; i < m; i++) {
            int x = sc.nextInt();
            int y = sc.nextInt();
            int z = sc.nextInt();
            int p = sc.nextInt();
            // 如果p==1，则可以认为x-y边权为0
            edges[i] = new Edge(x, y, p == 0 ? z : 0);
        }
        System.out.println(kruskal(edges, n));
    }

    //Kruskal算法
    private static int kruskal(Edge[] edges, int n) {
        int minWeight = 0;
        // 按照边权升序
        Arrays.sort(edges, Comparator.comparingInt(a -> a.weight));

        UFSet ufs = new UFSet(n + 1);//不用0，从1开始
        // 最先遍历出来是边权最小的边
        for (Edge edge : edges) {
            // 如果edge.from节点 和 edge.to节点 是同一个连通分量（即都在最小生成树中），则此时会产生环
            // 因此只有当edge.from节点 和 edge.to节点不在同一个连通分量时，才能合并（纳入最小生成树）
            if (ufs.find(edge.from) != ufs.find(edge.to)) {
                minWeight += edge.weight;
                ufs.union(edge.from, edge.to);
                if (ufs.count == 1) {
                    return minWeight;
                }
            }
        }
        return -1;
    }

    //并查集
    static class UFSet {
        int[] father;
        int count;

        //初始化
        public UFSet(int n) {
            this.father = new int[n];
            this.count = n - 1;//排除0
            for (int i = 0; i < n; i++) {
                father[i] = -1;
            }
        }

        //并
        public void union(int x, int y) {
            int rootX = this.find(x);
            int rootY = this.find(y);
            if (rootX == rootY) {
                return;
            }
            if (this.father[rootX] < this.father[rootY]) {
                this.father[rootX] += this.father[rootY];
                this.father[rootY] = rootX;
            } else {
                this.father[rootY] += this.father[rootX];
                this.father[rootX] = rootY;
            }
            this.count--;
        }

        //查
        public int find(int x) {
            int root = x;
            while (this.father[root] > 0) {
                root = this.father[root];
            }
            while (this.father[x] != root && this.father[x] > 0) {
                int next = this.father[x];
                father[x] = root;
                x = next;
            }
            return root;
        }
    }
}
