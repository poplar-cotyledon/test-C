package com.yzy.twohundred;

import java.util.HashMap;
import java.util.Scanner;

public class TroublesOfCourier_200_47 {
    static int n;
    static int[][] dist;//初始为邻接矩阵，dist[i][j]用于记录点i->j的最短距离
    static int[][] path;//初始全为-1，path[i][j]用于记录点i->j最短距离情况下需要经过的中转点
    static int result;//记录经过所有点后回到出发点的最短距离

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();//客户家与快递站的信息个数 5
        int m = sc.nextInt();//客户家与客户家的信息个数 1
        dist = new int[n + 1][n + 1];
        path = new int[n + 1][n + 1];
        //1.初始化：dist[][]对角线全为 0，其他全为 ∞，path[][]全为-1
        for (int i = 0; i < n + 1; i++) {
            for (int j = 0; j < n + 1; j++) {
                // 初始时默认i,j不相连，即i,j之间距离无穷大
                if (i != j) {
                    dist[i][j] = Integer.MAX_VALUE;
                }
                path[i][j] = -1;
            }
        }
        //2.建立点与点之间的关联
        //①快递站与客户家
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 1; i <= n; i++) {
            int id = sc.nextInt();//客户家序号
            int dis = sc.nextInt();//快递站到客户家的距离
            map.put(id, i);//离散化处理
            //建立关联，将∞替换为快递站到客户家i的距离
            dist[0][i] = dis;
            dist[i][0] = dis;
        }
        //②客户家与客户家
        for (int i = 1; i <= m; i++) {
            int id1 = map.get(sc.nextInt());
            ;//客户1家序号
            int id2 = map.get(sc.nextInt());
            ;//客户2家序号
            int dis = sc.nextInt();//客户1家到客户2家的距离
            //建立关联，将∞替换为客户1家到客户2家的距离
            dist[id1][id2] = dis;
            dist[id2][id1] = dis;
        }
        //3.floyd算法调用：求解图中任意两点之间的最短路径
        floyd();
        //4.全排列模拟经过所有点的路径，记录最短路径距离
        result = Integer.MAX_VALUE;
        dfs(0, 0, new boolean[n + 1], 0);
        System.out.println(result);
    }

    private static void floyd() {
        //考虑以Vk作为中转点
        for (int k = 0; k < n + 1; k++) {
            //遍历整个矩阵，i为行号，j为列号
            for (int i = 0; i < n + 1; i++) {
                for (int j = 0; j < n + 1; j++) {
                    int newDist = dist[i][k] + dist[k][j];
                    //如果以Vk为中转点的路径更短
                    if (dist[i][j] > newDist) {
                        //则更新最短路径长度
                        dist[i][j] = newDist;
                        //并在path[][]中记录中转点
                        path[i][j] = k;
                    }
                }
            }
        }
    }

    /**
     * 找一条经过所有点的最短路径，我们可以求解所有点形成的全排列，每一个全排列都对应一条经过所有点的路径，只是经过点的先后顺序不同
     * 求某个全排列过程中，可以通过dist数组，累计上一个点i到下一个点j的最短路径dist[i][j]
     *
     * @param pre   上一个点, 初始为0，表示从快递站出发
     * @param sum   当前全排列路径累计的路径权重 dist[0][1]+dist[1][2]+dist[2][3]+...+dist[i][0]
     * @param used  全排列used数组，用于标记哪些点已使用过
     * @param level 用于记录排列的长度
     */
    private static void dfs(int pre, int sum, boolean[] used, int level) {
        //结束条件：
        if (level == n) {
            //此时pre是最后一个客户所在点，送完最后一个客户后，快递员需要回到快递站，因此最终累计路径权重为 sum + dist[pre][0]
            result = Math.min(result, sum + dist[pre][0]);
            return;
        }
        //进入循环：0是快递站是起点，从1开始
        for (int i = 1; i <= n; i++) {
            if (used[i]) {
                continue;
            }
            used[i] = true;
            dfs(i, sum + dist[pre][i], used, level + 1);
            used[i] = false;
        }
    }
}