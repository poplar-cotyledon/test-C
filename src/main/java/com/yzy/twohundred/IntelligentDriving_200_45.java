package com.yzy.twohundred;

import java.util.*;

public class IntelligentDriving_200_45 {
    //记录路径中位置的几个状态
    static class Node {
        int x;//位置横坐标
        int y;//位置纵坐标
        int init;//到达此位置所需的最少初始油量
        int remain;//到达此位置时剩余可用油量
        boolean flag;//到达此位置前有没有加过油

        public Node(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    //右、下、左、上
    static int[][] offsets = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] split = sc.nextLine().split(",");
        int row = Integer.parseInt(split[0]);
        int col = Integer.parseInt(split[1]);
        int[][] matrix = new int[row][col];
        for (int i = 0; i < row; i++) {
            matrix[i] = Arrays.stream(sc.nextLine().split(",")).mapToInt(Integer::parseInt).toArray();
        }
        //如果左上角和右下角不可达，则直接返回-1
        if (matrix[0][0] == 0 || matrix[row - 1][col - 1] == 0) {
            System.out.println(-1);
        }

        //queue记录在第几步的所有位置信息
        LinkedList<Node> queue = new LinkedList<>();
        //path_init记录起点(0,0)到达(x,y)的所有可达路径中最优路径（即初始油量需求最少的路径）的初始油量
        int[][] path_init = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                path_init[i][j] = Integer.MAX_VALUE;
            }
        }
        //path_remain记录起点(0,0)到达(x,y)的所有可达路径中最优路径（即初始油量需求最少的路径）的最大剩余可用油量
        int[][] path_remain = new int[row][col];
        //path_flag记录起点(0,0)到达(x,y)的所有可达路径中是否经过加油站
        boolean[][] path_flag = new boolean[row][col];

        //起点
        Node start = new Node(0, 0);
        if (matrix[0][0] == -1) {
            //如果起点是加油站
            start.init = 0;
            start.remain = 100;
            start.flag = true;
        } else {
            //如果起点不是加油站
            start.init = matrix[0][0];
            start.remain = 0;
            start.flag = false;
        }
        queue.add(start);
        path_init[0][0] = start.init;
        path_remain[0][0] = start.remain;
        path_flag[0][0] = start.flag;

        //BFS
        while (queue.size() > 0) {
            Node cur = queue.removeFirst();
            //从当前位置cur开始向右、下、左、上四个方向探路
            for (int[] offset : offsets) {
                //1.得到可达新位置
                int newX = cur.x + offset[0];
                int newY = cur.y + offset[1];
                if (newX < 0 || newX >= row || newY < 0 || newY >= col || matrix[newX][newY] == 0) {
                    continue;
                }
                //2.如果新位置可达，则计算到达新位置的三个状态数据
                int init = cur.init;
                int remain = cur.remain;
                boolean flag = cur.flag;
                if (matrix[newX][newY] == -1) {
                    //① 如果新位置是加油站，则加满油
                    remain = 100;
                    flag = true;
                } else {
                    //② 如果新位置不是加油站，则需要消耗matrix[newX][newY]个油
                    remain -= matrix[newX][newY];
                }
                //3.如果到达新位置后，剩余油量为负数
                if (remain < 0) {
                    if (flag) {
                        //① 如果已加过油，则无法"借"油，跳过
                        continue;
                    } else {
                        //② 如果没加过油，则可以从初始油量里面"借"
                        init -= remain;
                        //如果借后的初始油量超过了满油100，则跳过
                        if (init > 100) {
                            continue;
                        }
                        remain = 0;
                    }
                }
                //4.如果当前路径策略到达新位置(newX,newY)所需初始油量init更少，
                //  或者当前路径策略到达新位置(newX,newY)剩余可用油量remain更多，
                //  或者remain和前面路径策略相同，但是当前路径策略未经过加油站
                if (init < path_init[newX][newY]
                        || remain > path_remain[newX][newY]
                        || (remain == path_remain[newX][newY] && flag != path_flag[newX][newY])) {
                    //① 则当前路径策略更优，记录更优路径的状态
                    path_init[newX][newY] = init;
                    path_remain[newX][newY] = remain;
                    path_flag[newX][newY] = flag;
                    //② 并将当前新位置加入BFS队列
                    Node next = new Node(newX, newY);
                    next.init = init;
                    next.remain = remain;
                    next.flag = flag;
                    queue.add(next);
                }
            }
        }

        //path_init[m - 1][n - 1] 记录的是到达右下角终点位置所需的最少初始油量
        System.out.println(path_init[row - 1][col - 1] == Integer.MAX_VALUE ? -1 : path_init[row - 1][col - 1]);
    }
}
