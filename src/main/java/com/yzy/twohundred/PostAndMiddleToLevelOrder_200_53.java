package com.yzy.twohundred;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class PostAndMiddleToLevelOrder_200_53 {
    //广度优先搜索的执行队列，先加入左子树，再加入右子树，先加入post，再加入middle
    static LinkedList<String[]> queue = new LinkedList<>();
    //层序遍历出来的元素存放在result中
    static List<Character> result = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] orders = sc.nextLine().split(" ");

        queue.add(orders);
        while (queue.size() > 0) {
            String[] tmp = queue.removeFirst();
            String post = tmp[0];
            String middle = tmp[1];
            //1.后序遍历的最后一个元素就是root，计入result
            char root = post.charAt(post.length() - 1);
            result.add(root);
            //2.得到root在中序遍历里的下标rootIdx
            int rootIdx = middle.indexOf(root);
            //3.得到root的左子树长度
            int leftLen = rootIdx;
            //4.如果存在左子树
            if (leftLen > 0) {
                //① 从后序遍历中，截取出左子树的后序遍历
                String leftPost = post.substring(0, leftLen);
                //② 从中序遍历中，截取出左子树的中序遍历
                String leftMid = middle.substring(0, rootIdx);
                //③ 将左子树的后、中遍历序列加入queue
                queue.add(new String[]{leftPost, leftMid});
            }
            //5.如果存在右子树
            if (post.length() - 1 - leftLen > 0) {
                //① 从后序遍历中，截取出右子树的后序遍历
                String rightPost = post.substring(leftLen, post.length() - 1);
                //② 从中序遍历中，截取出右子树的中序遍历
                String rightMid = middle.substring(rootIdx + 1);
                //③ 将右子树的后、中遍历序列加入执行队列
                queue.addLast(new String[]{rightPost, rightMid});
            }
        }

        //6.遍历输出
        StringBuilder sb = new StringBuilder();
        for (Character c : result) {
            sb.append(c);
        }
        System.out.println(sb);
    }
}
