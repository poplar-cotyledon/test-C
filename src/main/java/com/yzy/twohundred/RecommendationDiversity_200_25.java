package com.yzy.twohundred;

import java.util.*;

public class RecommendationDiversity_200_25 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int col = Integer.parseInt(sc.nextLine());//窗口数量——列
        int row = Integer.parseInt(sc.nextLine());//每个窗口的元素数量——行
        List<LinkedList<Integer>> list = new ArrayList<>();//待分配元素集合list
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            //本地测试，以空行作为输入截止条件
            if (s.length() == 0) {
                break;
            }
            Integer[] nums = Arrays.stream(s.split(" ")).map(Integer::parseInt).toArray(Integer[]::new);
            list.add(new LinkedList<>(Arrays.asList(nums)));
        }

        int[][] windows = new int[row][col];
        int level = 0;//指示list行
        for (int i = 0; i < row; i++) {
            boolean flag = false;//当前轮次是否发生了"借"动作
            for (int j = 0; j < col; j++) {
                windows[i][j] = list.get(level).removeFirst();
                //如果list的这一行分配完，且list中除了这个已分配的一行外，还有一行，则可发生"借"的动作
                if (list.get(level).size() == 0 && list.size() > 1) {
                    list.remove(level);//删除空list行
                    level %= list.size();//防止越界
                    flag = true;//发生"借"的动作
                }
            }
            //如果未发生"借"的动作，则需要切到list的下一行
            if (!flag) {
                level = (level + 1) % list.size();//防止越界
            }
        }

        StringJoiner sj = new StringJoiner(" ");
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < row; j++) {
                sj.add(windows[j][i]+"");
            }
        }
        System.out.println(sj);
    }
}
