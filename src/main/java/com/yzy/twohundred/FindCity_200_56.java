package com.yzy.twohundred;

import java.util.*;

public class FindCity_200_56 {
    //并查集实现：
    //存储结构——树的双亲表示法，用一个数组即可表示集合关系
    static class UFSet {
        int[] father;

        //初始化
        public UFSet(int n) {
            //数组值为-1的元素，即为根结点
            this.father = new int[n];
            for (int i = 1; i < n; i++) {
                father[i] = -1;
            }
        }

        //并 +优化(让树不长高)
        public void union(int x, int y) {
            int rootX = this.find(x);//找到x的根结点rootX
            int rootY = this.find(y);//找到y的根结点rootY
            //若根结点相同，则返回
            if (rootX == rootY) {
                return;
            }
            //father[]<0,越小，说明包含结点越多
            if (father[rootX] >= father[rootY]) {
                //以rootY为根
                this.father[rootY] += this.father[rootX];//累加结点总数
                this.father[rootX] = rootY;//小树合并到大树
            } else {
                //以rootX为根
                this.father[rootX] += this.father[rootY];//累加结点总数
                this.father[rootY] = rootX;//小树合并到大树
            }
        }

        //查 +优化(压缩路径)
        public int find(int x) {
            int root = x;
            //循环寻找x所属的最终根结点
            while (father[root] > 0) {
                root = father[root];
            }
            //将循环找根结点的路径上，所有结点的父结点置为根结点
            while (father[x] != root && father[x] > 0) {
                int temp = father[x];
                father[x] = root;//x直接挂在根结点下
                x = temp;
            }
            return root;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] city = new int[n - 1][2];
        for (int i = 0; i < n - 1; i++) {
            city[i][0] = sc.nextInt();
            city[i][1] = sc.nextInt();
        }
        getResult(city, n);
        List<Integer> DPList = new ArrayList<>(DPMap.keySet());
        StringJoiner sj = new StringJoiner(" ");
        for (Integer key : DPList) {
            if (DPMap.get(key) == minDP) {
                sj.add(key + "");
            }
        }
        System.out.println(sj);
    }

    static Map<Integer, Integer> DPMap = new HashMap<>();
    static int minDP = Integer.MAX_VALUE;

    private static void getResult(int[][] city, int n) {
        //遍历切断城市i
        for (int i = 1; i <= n; i++) {
            int maxDP = 0;//城市i的DP，取连通分量最大者
            //使用并查集
            UFSet ufs = new UFSet(n + 1);//0舍弃，用1~n
            //用union关联城市
            for (int[] c : city) {
                int x = c[0];
                int y = c[1];
                if (x != i && y != i) {
                    ufs.union(x, y);
                }
            }
            //遍历father数组，若值>0，则跳过，若值<0,则取绝对值计入maxDP
            for (int j = 1; j <= n; j++) {
                if (ufs.father[j] < 0) {
                    maxDP = Math.max(maxDP, Math.abs(ufs.father[j]));
                }
            }
            DPMap.put(i, maxDP);
            minDP = Math.min(minDP, maxDP);
        }
    }
}
