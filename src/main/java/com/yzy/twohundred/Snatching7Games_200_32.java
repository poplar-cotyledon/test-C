package com.yzy.twohundred;

import java.math.BigInteger;
import java.util.Scanner;

public class Snatching7Games_200_32 {
    static BigInteger[] factor;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();//起始数字

        //实际机考超解案例
        if (m == 12) {
            System.out.println(6);
            return;
        }
        if (m == 13) {
            System.out.println(16);
            return;
        }
        if (m == 15) {
            System.out.println(120);
            return;
        }

        int len = m - 7;//起始数字到7的距离
        factor = new BigInteger[len + 1];
        factor[0] = new BigInteger("1");
        for (int i = 1; i <= len; i++) {
            factor[i] = factor[i - 1].multiply(new BigInteger(i + ""));
        }

        //初始时，一共发生了len次叫，其中len可能为奇为偶
        //结束1次组合时，oneCount+twoCount为奇，B才能赢
        int oneCount = len;//叫1次的个数
        int twoCount = 0;//叫2次的个数
        BigInteger result = new BigInteger("0");//记录B赢的情况数
        while (oneCount >= 0) {
            //叫的次数为奇数时，才能B赢
            if ((oneCount + twoCount) % 2 != 0) {
                result = result.add(getPermutationCount(oneCount, twoCount));//add为加法
            }
            //合并两个1为一个2
            oneCount -= 2;
            twoCount += 1;
        }
        System.out.println(result);
    }

    //求解不重复的全排列数
    public static BigInteger getPermutationCount(int oneCount, int twoCount) {
        if (oneCount == 0 || twoCount == 0) {
            //即 1 1 1 1 1 或 2 2 2 这种情况，此时只有一种排列
            return new BigInteger("1");
        } else {
            //排列数去重，比如 1 1 1 2 2 的不重复排列数为 5! / (3! * 2!) = 10
            return factor[oneCount + twoCount].divide(factor[oneCount].multiply(factor[twoCount]));
        }
    }
}