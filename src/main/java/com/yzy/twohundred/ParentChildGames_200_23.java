package com.yzy.twohundred;

import java.util.LinkedList;
import java.util.Scanner;

public class ParentChildGames_200_23 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] matrix = new int[n][n];
        int[][] path = new int[n][n];
        LinkedList<Integer> queue = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = sc.nextInt();
                path[i][j] = -1;
                if (matrix[i][j] == -3) {
                    path[i][j] = 0;
                    queue.add(i * n + j);//二维坐标一维化
                }
            }
        }

        int[][] offsets = {{1, 0}, {0, -1}, {-1, 0}, {0, 1}};//下、左、上、右
        int result = -1;
        //BFS按层扩散
        while (queue.size() > 0) {
            LinkedList<Integer> newQueue = new LinkedList<>();//记录当前扩散层的点
            boolean flag = false;//当前层是否有宝宝所在的点
            //1.遍历所有起点
            for (Integer start : queue) {
                //起点坐标二维化
                int x = start / n;
                int y = start % n;
                //2.向下、左、上、右四方扩散
                for (int[] offset : offsets) {
                    int newX = x + offset[0];
                    int newY = y + offset[1];
                    //如果扩散点越界，或为障碍物，则无法扩散
                    if (newX < 0 || newX >= n || newY < 0 || newY >= n || matrix[newX][newY] == -1) {
                        continue;
                    }
                    //3.如果扩散点的path值为-1，说明还未扩散
                    //① 需要将坐标一维化加入newQueue
                    if (path[newX][newY] == -1) {
                        newQueue.add(newX * n + newY); // 加入当前扩散层
                    }
                    //② 需要更新path值，取最大值
                    path[newX][newY] = Math.max(path[newX][newY], path[x][y] + Math.max(0, matrix[newX][newY]));
                    //4.如果扩散点是宝宝的位置，则可停止往下一层扩散，因为已经找到宝宝的最短路径长度（即扩散层数）
                    if (matrix[newX][newY] == -2) {
                        result = path[newX][newY];
                        flag = true;
                    }
                }
            }
            //5.遍历完所有起点后，已经找到去宝宝位置的最短路径和最大糖果数，则终止BFS
            if (flag) {
                break;
            }
            //6.否则继续往下一层扩散
            queue = newQueue;
        }
        System.out.println(result);
    }
}
