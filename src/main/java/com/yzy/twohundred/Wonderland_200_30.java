package com.yzy.twohundred;

import java.util.Arrays;
import java.util.Scanner;

public class Wonderland_200_30 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] costs = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int[] days = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        int[] dp = new int[366];
        for (int i = 1; i < 366; i++) {
            dp[i] = Integer.MAX_VALUE;
        }
        int j = 0;//控制days数组
        //遍历365天，逐日累加金额
        for (int i = 1; i <= 365; i++) {
            if (j < days.length && i == days[j]) {
                dp[i] = Math.min(dp[i], dp[Math.max(0, i - 1)] + costs[0]);//买一日票
                dp[i] = Math.min(dp[i], dp[Math.max(0, i - 3)] + costs[1]);//买三日票
                dp[i] = Math.min(dp[i], dp[Math.max(0, i - 7)] + costs[2]);//买一周票
                dp[i] = Math.min(dp[i], dp[Math.max(0, i - 30)] + costs[3]);//买一月票
                j++;
            } else {
                dp[i] = dp[i - 1];
            }
        }
        System.out.println(dp[365]);
    }
}