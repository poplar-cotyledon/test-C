package com.yzy.twohundred;

import java.util.Arrays;
import java.util.Scanner;

public class TeamProgram_200_11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        int[] levels = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        //暴力枚举，遍历找到左/右边比自己大/小的元素的个数
        int result = 0;
        for (int i = 1; i < n - 1; i++) {
            int mid = levels[i];
            long leftBiggerCount = 0;//左边比自己大的元素的个数
            long leftSmallerCount = 0;//左边比自己小的元素的个数
            for (int j = 0; j < i; j++) {
                if (levels[j] > mid) {
                    leftBiggerCount++;
                } else {
                    leftSmallerCount++;
                }
            }
            long rightBiggerCount = 0;//右边比自己大的元素的个数
            long rightSmallerCount = 0;//右边比自己小的元素的个数
            for (int j = i + 1; j < n; j++) {
                if (levels[j] > mid) {
                    rightBiggerCount++;
                } else {
                    rightSmallerCount++;
                }
            }
            result += leftBiggerCount * rightSmallerCount + leftSmallerCount * rightBiggerCount;
        }
        System.out.println(result);
    }
}
