package com.yzy.twohundred;

import java.util.Arrays;
import java.util.Scanner;

public class DepartmentManpowerAllocation_200_10 {
    static int monthNum;//需要在monthNum个月内完成开发完
    static int[] requirements;//requirements[i]表示第i个需求的工作量大小

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        monthNum = Integer.parseInt(sc.nextLine());
        requirements = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        System.out.println(getResult());
    }

    public static long getResult() {
        Arrays.sort(requirements);
        int n = requirements.length;
        long min = requirements[n - 1];//每个人的工作量 至少是 需求数组中最大的工作量
        long max = requirements[n - 2] + requirements[n - 1];//每个人的工作量 至多是 需求数组中最大和次大的那两个工作量
        long result = max;
        //二分取中间值
        while (min <= max) {
            long mid = (min + max) >> 1; //需要注意的是，min，max单独看都不超过int，但是二者相加会超过int，因此需要用long类型
            if (check(mid)) {
                //如果每个月的最大工作量为mid，可以满足在monthNum个月内完成开发完，则mid就是一个可能解，但不一定是最优解
                result = mid;
                //继续尝试每个月的更小的工作量，即缩小右边界
                max = mid - 1;
            } else {
                //如果每个月的最大工作量为mid，不能满足在monthNum个月内完成开发完，则mid限重小了，我们应该尝试更大的限重，即扩大左边界
                min = mid + 1;
            }
        }
        return result;
    }

    /**
     * @param limit 每个月的最大工作量
     * @return 每个月的最大工作量为limit的情况下，能否在monthNum个月内完成开发完
     */
    public static boolean check(long limit) {
        int l = 0;//指向工作量最少的需求
        int r = requirements.length - 1;//指向工作量最多的需求
        int need = 0;//需要的月数
        while (l <= r) {
            //如果工作量最少的需求和工作量最多的需求可以在一个月内完成，则l++,r--，
            //否则工作量最多的需求只能单独一个月完成，即仅r--
            if (requirements[l] + requirements[r] <= limit) {
                l++;
            }
            r--;
            //用掉一个月
            need++;
        }
        //如果monthNum >= need，则能完成
        return monthNum >= need;
    }
}