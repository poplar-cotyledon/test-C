package com.yzy.twohundred;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class HappyWeekend_200_52 {
    //并查集实现：
    //存储结构——树的双亲表示法，用一个数组即可表示集合关系
    static class UFSet {
        int[] father;

        //初始化
        public UFSet(int n) {
            //数组值为-1的元素，即为根结点
            this.father = new int[n];
            for (int i = 0; i < n; i++) {
                father[i] = -1;
            }
        }

        //并 +优化(让树不长高)
        public void union(int x, int y) {
            int rootX = this.find(x);//找到x的根结点rootX
            int rootY = this.find(y);//找到y的根结点rootY
            //若根结点相同，则返回
            if (rootX == rootY) {
                return;
            }
            //father[]<0,越小，说明包含结点越多
            if (father[rootX] >= father[rootY]) {
                //以rootY为根
                this.father[rootY] += this.father[rootX];//累加结点总数
                this.father[rootX] = rootY;//小树合并到大树
            } else {
                //以rootX为根
                this.father[rootX] += this.father[rootY];//累加结点总数
                this.father[rootY] = rootX;//小树合并到大树
            }
        }

        //查 +优化(压缩路径)
        public int find(int x) {
            int root = x;
            //循环寻找x所属的最终根结点
            while (father[root] > 0) {
                root = father[root];
            }
            //将循环找根结点的路径上，所有结点的父结点置为根结点
            while (father[x] != root && father[x] > 0) {
                int temp = father[x];
                father[x] = root;//x直接挂在根结点下
                x = temp;
            }
            return root;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int hang = sc.nextInt();
        int lie = sc.nextInt();
        int[][] map = new int[hang][lie];
        for (int i = 0; i < hang; i++) {
            for (int j = 0; j < lie; j++) {
                map[i][j] = sc.nextInt();
            }
        }
        System.out.println(getResult(hang, lie, map));
    }

    private static int getResult(int hang, int lie, int[][] map) {
        List<Integer> hw = new ArrayList<>();//记录小华，小为的位置
        List<Integer> restaurants = new ArrayList<>();//记录餐厅的位置
        int[][] offset = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};//上下左右的偏移量
        UFSet ufSet = new UFSet(hang * lie);//构建并查集
        for (int x = 0; x < hang; x++) {
            for (int y = 0; y < lie; y++) {
                if (map[x][y] != 1) {
                    int loc = x * lie + y;//二维坐标(x,y)转一维坐标loc
                    if (map[x][y] == 2) {
                        hw.add(loc);
                    } else if (map[x][y] == 3) {
                        restaurants.add(loc);
                    }
                    //关联
                    for (int[] o : offset) {
                        int newX = x + o[0];
                        int newY = y + o[1];
                        if (newX >= 0 && newX < hang &&
                                newY >= 0 && newY < lie &&
                                map[newX][newY] != 1) {
                            // 如果(x,y)和(newX,newY)位置都是非1，则合并
                            ufSet.union(loc, newX * lie + newY);
                        }
                    }
                }
            }
        }
        System.out.println(Arrays.toString(ufSet.father));
        int rootH = ufSet.find(hw.get(0));//小华根结点的一维坐标
        int rootW = ufSet.find(hw.get(1));//小为根结点的一维坐标
        //小华和小为不属于同一个连通分量，无法去往相同的餐厅
        if (rootH != rootW) {
            return 0;
        }
        int result = 0;
        for (Integer r : restaurants) {
            if (ufSet.find(r) == rootH) {
                result++;
            }
        }
        return result;
    }
}
