package com.yzy.twohundred;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class MaxLengthOfSubstrings2_200_43 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char[] chars = sc.nextLine().toCharArray();

        List<LinkedList<Integer>> list = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            list.add(new LinkedList<>());
        }
        list.get(0).add(-1);

        int status = 0b000;
        int maxLength = 0;
        for (int i = 0; i < chars.length * 2; i++) {
            char c = chars[i % chars.length];
            switch (c) {
                case 'l' -> status ^= 0b100;
                case 'o' -> status ^= 0b010;
                case 'x' -> status ^= 0b001;
            }
            //第一轮需要记录该状态出现过的所有位置
            LinkedList<Integer> loc = list.get(status);
            if (i < chars.length) {
                loc.add(i);
            }
            while (loc.size() > 0) {
                int earliest = loc.getFirst();
                //第二轮需要移除子串大小超过主串的情况
                int length = i - earliest;
                if (length > chars.length) {
                    loc.removeFirst();
                } else {
                    maxLength = Math.max(maxLength, length);
                    break;
                }
            }
        }
        System.out.println(maxLength);
    }
}
