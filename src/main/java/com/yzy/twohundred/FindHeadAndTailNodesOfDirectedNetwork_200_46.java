package com.yzy.twohundred;

import java.util.*;

public class FindHeadAndTailNodesOfDirectedNetwork_200_46 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if (n == 0) {
            System.out.println(-1);
            return;
        }
        Map<Integer, Integer> inDegreeMap = new HashMap<>();//存入度
        Map<Integer, List<Integer>> nextMap = new HashMap<>();//存后继结点
        Set<Integer> set = new HashSet<>();//存图中所有点（不重复）
        for (int i = 0; i < n; i++) {
            int from = sc.nextInt();
            int to = sc.nextInt();
            set.add(from);
            set.add(to);
            inDegreeMap.put(from, inDegreeMap.getOrDefault(from, 0));//from入度不变
            inDegreeMap.put(to, inDegreeMap.getOrDefault(to, 0) + 1);//to入度+1
            nextMap.putIfAbsent(from, new ArrayList<>());
            nextMap.get(from).add(to);
        }

        //1.收集第一层入度为0的点
        List<Integer> list = new ArrayList<>();
        //2.记录任务执行的起点与终点
        StringJoiner result = new StringJoiner(" ");
        for (Integer key : inDegreeMap.keySet()) {
            if (inDegreeMap.get(key) == 0) {
                list.add(key);
                result.add(key + "");//首节点
            }
        }
        //3.记录终点
        List<Integer> lastList = new ArrayList<>();
        //4.初始为排除起点的所有点个数，剥夺一个count--，如果图中存在环，则必然最终count>0
        int count = set.size() - 1;
        //5.
        while (list.size() > 0) {
            //收集新一层入度为0的点
            List<Integer> newList = new ArrayList<>();
            //收集新一层没有后继结点的点
            for (Integer key : list) {
                List<Integer> nextNodes = nextMap.get(key);
                if (nextNodes == null) {
                    lastList.add(key);
                } else {
                    for (Integer nextNode : nextNodes) {
                        inDegreeMap.put(nextNode, inDegreeMap.get(nextNode) - 1);
                        if (inDegreeMap.get(nextNode) == 0) {
                            newList.add(nextNode);
                            count--;
                        }
                    }
                }
            }
            list = newList;
        }
        if (count > 0) {
            System.out.println(-1);
            return;
        }
        lastList.sort(Comparator.comparingInt(o -> o));
        for (Integer last : lastList) {
            result.add(last + "");
        }
        System.out.println(result);
    }
}
