package com.yzy.twohundred;

import java.util.PriorityQueue;
import java.util.Scanner;

public class FindingBestRoadTestRoute_200_33 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt();
        int col = sc.nextInt();

        //① 邻接矩阵：记录每一格的权值
        int[][] matrix = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }
        //② dist[i][j]记录起点(0,0)到终点(i,j)的所有路径中“最大的”最小权值节点的权值，由于本题节点的最小权值>=0，因此这里可以初始化为0
        int[][] dist = new int[row][col];
        dist[0][0] = matrix[0][0];//起点0到终点0路径的最小权值节点就是自身，即matrix[0][0]点的权重
        //③ 优先队列：记录路径（终点），并且路径中的最小权值节点的权值越大，优先级越高
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> dist[b[2]][b[3]] - dist[a[2]][a[3]]);
        pq.add(new int[]{-1, -1, 0, 0});//初始时将(0,0)入队
        //④ 下、右、上、左的方向偏移量
        int[][] offsets = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

        while (pq.size() > 0) {
            //取出优先队列中优先级最大的路径（终点）——一维坐标，初始为起点0
            int[] cur = pq.poll();
            int preX = cur[0];
            int preY = cur[1];
            int curX = cur[2];
            int curY = cur[3];
            //若已找到dist[r-1][c-1]最优解，则可以提前结束
            if (curX == row - 1 && curY == col - 1) {
                break;
            }
            //若未找到dist[r-1][c-1]最优解，则向下、右、上、左四个方向探索
            for (int[] offset : offsets) {
                //新位置
                int newX = curX + offset[0];
                int newY = curY + offset[1];
                //新位置越界则无法访问
                if (newX < 0 || newX >= row || newY < 0 || newY >= col) {
                    continue;
                }
                //剪枝：不允许掉头，因此新位置处于掉头位置的话，不可进入
                if (newX == preX && newY == preY) {
                    continue;
                }
                //🐱：保留该路径上的最小权值
                int weight = Math.min(dist[curX][curY], matrix[newX][newY]);
                //如果weight>dist[newX][newY]，则说明我们找到了起点(0,0)到(newX,newY)的更优路径，即找到了"更大的最小权值节点"
                if (weight>dist[newX][newY]) {
                    //则需要更新dist[newX][newY]
                    dist[newX][newY] = weight;
                    //然后将新路径加入到pq中重新排优先级。
                    pq.add(new int[]{curX, curY, newX, newY});
                }
            }
        }
        // 返回起点（0，0）到终点(r-1, c-1)的所有路径中"最大的"最小权值节点的权值
        System.out.println(dist[row - 1][col - 1]);
    }
}
