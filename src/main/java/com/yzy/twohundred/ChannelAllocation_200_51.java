package com.yzy.twohundred;

import java.util.Arrays;
import java.util.Scanner;

public class ChannelAllocation_200_51 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int R = sc.nextInt();//最大阶数5               1 2 4 8 16 32
        int[] N = new int[R + 1];//信道数量            10 5 0 1 3  2
        for (int i = 0; i <= R; i++) {
            N[i] = sc.nextInt();
        }
        int D = sc.nextInt();//单个用户需要传输的数据量47：1 1 1 1 0  1
        System.out.println(getResult(R, N, D));
    }

    public static int getResult(int R, int[] N, int D) {
        // 将D值转化为二进制形式，并且为了和N[]的阶位进行对应，这里将D的二进制进行了反转
        StringBuilder sb = new StringBuilder(Integer.toBinaryString(D));//101111
        String s = sb.reverse().toString();//111101
        int[] subtrahend = Arrays.stream(s.split("")).mapToInt(Integer::parseInt).toArray();//减数数组：1 1 1 1 0 1

        // count记录N能承载几个D
        int count = 0;//无高阶信道一次性囊括
        // N中高阶信道的单个信道就能满足D，因此这些高阶信道有几个，即能承载几个D
        for (int i = R; i >= subtrahend.length; i--) {
            // R ~ subtrahend.length 阶的单个信道就能承载一个D，因此这些信道有几个，就能承载几个D
            count += N[i];
        }

        // 0 ~ subtrahend.length - 1 阶的单个信道无法承载一个D，因此这些阶需要组合起来才能承载一个D
        // 注：Arrays.copyOfRange(数组，from，to)——包含from，不包含to
        int[] minuend = Arrays.copyOfRange(N, 0, subtrahend.length);//被减数数组：10 5 0 1 3 2
        // 进行二进制减法
        while (binary_sub(minuend, subtrahend)) {
            count++;
        }
        return count;
    }

    /**
     * 二进制减法
     *                  信道容量：1  2 4 8 16 32
     * @param minuend    被减数：10 5 0 1 3 2
     * @param subtrahend 减数：  1 1 1 1 0 1
     * @return 被减数是否为正数?
     */
    public static boolean binary_sub(int[] minuend, int[] subtrahend) {
        // 进行减法运算逻辑, 从高位开始
        for (int i = minuend.length - 1; i >= 0; i--) {
            if (minuend[i] >= subtrahend[i]) {
                // 一、如果对应位的信道数足够，则直接相减
                minuend[i] -= subtrahend[i];
            } else {
                // 二、如果对应位的信道数不足，此时有两种策略，一是向低位借，一是向高位借
                // 具体向哪里借，需要看 minuend 的 [0,i] 低位部分是否能够承载 subtrahend[0, i] 低位部分
                // 注：Arrays.copyOfRange(数组，from，to)——包含from，不包含to
                if (sum(Arrays.copyOfRange(minuend, 0, i + 1)) < sum(Arrays.copyOfRange(subtrahend, 0, i + 1))) {
                    // 1.如果 minuend 的 [0,i]不能承载，则向高位借，即从j=i+1位开始借
                    int j = i + 1;
                    while (j < minuend.length) {
                        if (minuend[j] > 0) {
                            // 如果高位 j 有信道可借，则借
                            minuend[j] -= 1;
                            return true;
                        } else {
                            // 否则继续向更高位探索
                            j++;
                        }
                    }
                    // 如果所有高位都没有富余信道数，则说明减法结果为负数
                    return false;
                } else {
                    // 2. 如果minuend 的 [0,i]可以承载，则向低位借(向低位借，可以避免浪费)
                    // 此时minuend[i]为负数，表示欠债
                    minuend[i] -= subtrahend[i];
                    // 将当前阶位的欠债，转移到前面的低阶位上，注意转移时，欠债x2
                    minuend[i - 1] += minuend[i] << 1;
                    // 转移后，当前阶位的欠债变为0
                    minuend[i] = 0;
                }
            }
        }
        return true;
    }

    public static int sum(int[] bin) {
        int ans = 0;
        for (int i = 0; i < bin.length; i++) {
            ans += bin[i] * (1 << i);// 左移相当于*2
        }
        return ans;
    }
}