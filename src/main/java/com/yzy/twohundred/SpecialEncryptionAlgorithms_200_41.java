package com.yzy.twohundred;

import java.util.*;

public class SpecialEncryptionAlgorithms_200_41 {
    //明文数字个数
    static int n;
    //明文
    static int[] data;
    //密码本矩阵大小
    static int m;
    //密码本
    static int[][] secret;
    //上 < 左 < 右 < 下
    static int[][] offsets = {{-1, 0}, {0, -1}, {0, 1}, {1, 0}};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        data = new int[n];
        for (int i = 0; i < n; i++) {
            data[i] = sc.nextInt();
        }
        m = sc.nextInt();
        secret = new int[m][m];
        //记录密码本中元素值等于“明文第一个数字”的所有元素的位置
        List<Integer> starts = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                secret[i][j] = sc.nextInt();
                if (data[0] == secret[i][j]) {
                    starts.add(i * m + j);
                }
            }
        }

        for (Integer start : starts) {
            //出发位置
            int x = start / m;
            int y = start % m;
            //used[i][j]用于记录密码本(i,j)元素是否已使用
            boolean[][] used = new boolean[m][m];
            used[x][y] = true;
            //记录结果路径各节点位置
            LinkedList<String> path = new LinkedList<>();
            path.add(x + " " + y);
            //开始深搜
            if (dfs(x, y, 1, path, used)) {
                StringJoiner sj = new StringJoiner(" ");
                for (String pos : path) {
                    sj.add(pos);
                }
                System.out.println(sj);
                return;
            }
        }
        System.out.println("error");
    }

    /**
     * @param x     当前位置横坐标
     * @param y     当前位置纵坐标
     * @param index data[index]是将要匹配的明文数字
     * @param path  路径
     * @param used  密码本各元素使用情况
     * @return 是否找到符合要求的路径
     */
    public static boolean dfs(int x, int y, int index, LinkedList<String> path, boolean[][] used) {
        //结束条件：
        if (index == n) {
            return true;
        }
        //循环条件：上 < 左 < 右 < 下
        for (int[] offset : offsets) {
            int newX = x + offset[0];
            int newY = y + offset[1];
            //如果新位置越界，或者新位置已使用，或者新位置不是目标值，则跳过
            if (newX < 0 || newX >= m || newY < 0 || newY >= m || used[newX][newY] || secret[newX][newY] != data[index]) {
                continue;
            }
            //递归进入新位置
            path.add(newX + " " + newY);
            used[newX][newY] = true;
            //① 如果当前分支可以找到符合要求的路径，则返回
            if (dfs(newX, newY, index + 1, path, used)) {
                return true;
            }
            //② 否则回溯
            used[newX][newY] = false;
            path.removeLast();
        }
        return false;
    }
}
