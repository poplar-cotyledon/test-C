package com.yzy.twohundred;

import java.util.*;

public class SSCGame_200_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Map<Character, List<String>> map = new HashMap<>();
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            //本地测试解开此行
            if (s.equals("")) {
                break;
            }
            String[] split = s.split(" ");
            char gesture = split[1].charAt(0);
            //如果有人不按套路出，则此局作废
            if (gesture < 'A' || gesture > 'C') {
                System.out.println("NULL");
                return;
            }
            //统计各个手势的出派人
            map.putIfAbsent(gesture, new ArrayList<>());
            map.get(gesture).add(split[0]);
        }

        switch (map.size()) {
            case 1, 3 ->
                    //只有一种手势，或者三种手势都有，则平局
                    System.out.println("NULL");
            case 2 -> {
                List<String> result;
                if (!map.containsKey('A')) {
                    //没有A手势，只有B、C手势，则B赢
                    result = map.get('B');
                } else if (!map.containsKey('B')) {
                    //没有B手势，只有A、C手势，则C赢
                    result = map.get('C');
                } else {
                    //没有C手势，只有A、B手势，则A赢
                    result = map.get('A');
                }
                result.sort(String::compareTo);
                result.forEach(System.out::println);
            }
        }
    }
}
