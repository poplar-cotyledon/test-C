package com.yzy.twohundred;

import java.util.Scanner;

public class ParkVisitPath_200_9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt();
        int col = sc.nextInt();
        int[][] park = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                park[i][j] = sc.nextInt();
            }
        }

        if (park[0][0] == 1 || park[row - 1][col - 1] == 1) {
            System.out.println(0);
            return;
        }

        long[][] dp = new long[row][col];
        dp[0][0] = 1;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (park[i][j] == 1) {
                    continue;
                }
                if (i > 0) {
                    dp[i][j] += dp[i - 1][j];
                }
                if (j > 0) {
                    dp[i][j] += dp[i][j - 1];
                }
            }
        }
        System.out.println(dp[row - 1][col - 1]);
    }
}
