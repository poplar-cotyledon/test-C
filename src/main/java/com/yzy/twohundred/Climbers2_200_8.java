package com.yzy.twohundred;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Climbers2_200_8 {
    // 输入处理
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] heights = Arrays.stream(sc.nextLine().split(",")).mapToInt(Integer::parseInt).toArray();
        int power = Integer.parseInt(sc.nextLine());
        System.out.println(getResult(heights, power));
    }

    // 算法实现（本题实际考试为核心代码模式，因此考试时只需要写出此函数实现即可）
    public static int getResult(int[] heights, int power) {
        //记录可攀登的山峰索引
        Set<Integer> idx = new HashSet<>();
        //正向攀登
        climb(heights, power, idx, true);
        //逆序攀登
        reverse(heights);
        climb(heights, power, idx, false);
        return idx.size();
    }

    private static void climb(int[] heights, int power, Set<Integer> idx, boolean flag) {
        //找到第一个地面位置
        int j = 0;
        while (j < heights.length && heights[j] != 0) {
            j++;
        }
        //攀登体力总消耗（包括上山，下山）
        int cost = 0;
        //开始攀登
        for (int i = j + 1; i < heights.length; i++) {
            //如果遇到了新的地面，则从新的地面位置重新计算攀登消耗的体力
            if (heights[i] == 0) {
                cost = 0;
                continue;
            }
            //diff记录高度差
            int diff = heights[i] - heights[i - 1];
            if (diff > 0) {
                //1.如果过程是上坡——上 + 原路返回下
                cost += diff * 3;
                //由于 height[i] > heights[i-1]，因此如果 height[i] > heights[i+1] 的话，位置 i 就是山顶
                if (i + 1 >= heights.length || heights[i] > heights[i + 1]) {
                    //计算攀登此山顶的上山下山消耗的体力和
                    if (cost < power) {
                        //如果小于自身体力，则可以攀登
                        if (flag) {
                            //正序攀登
                            idx.add(i);
                        } else {
                            //逆序攀登：对于的山峰位置需要反转
                            idx.add(heights.length - i - 1);
                        }
                    }
                }
            } else if (diff < 0) {
                //2.如果过程是下坡（负负得正）
                cost -= diff * 3;
            }
        }
    }

    private static void reverse(int[] heights) {
        int i = 0;
        int j = heights.length - 1;
        while (i < j) {
            int tmp = heights[i];
            heights[i] = heights[j];
            heights[j] = tmp;
            i++;
            j--;
        }
    }
}