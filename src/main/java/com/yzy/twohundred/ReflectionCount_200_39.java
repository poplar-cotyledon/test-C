package com.yzy.twohundred;

import java.util.Scanner;

public class ReflectionCount_200_39 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int col = sc.nextInt();//列
        int row = sc.nextInt();//行
        int y = sc.nextInt();//起始位置，第y列
        int x = sc.nextInt();//起始位置，第x行
        int sy = sc.nextInt();//初始速度，沿x方向
        int sx = sc.nextInt();//初始速度，沿y方向
        int t = sc.nextInt();//经过的时间
        char[][] matrix = new char[row][col];
        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = sc.next().toCharArray();
        }

        int count = 0;//记录1的个数
        while (t >= 0) {
            if (matrix[x][y] == '1') {
                count++;
            }
            x += sx;
            y += sy;
            //判断是否越界
            if (x < 0) {
                x = 1;
                sx = -sx;
            } else if (x >= row) {
                x = row - 2;
                sx = -sx;
            }
            if (y < 0) {
                y = 1;
                sy = -sy;
            } else if (y >= col) {
                y = col - 2;
                sy = -sy;
            }
            t--;
        }
        System.out.println(count);
    }
}
