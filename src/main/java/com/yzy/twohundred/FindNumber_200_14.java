package com.yzy.twohundred;

import java.util.Scanner;

public class FindNumber_200_14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        String s = "0" + Integer.toBinaryString(n);
        char[] chars = s.toCharArray();
        int count = 0;
        for (int i = chars.length - 2; i >= 0; i--) {
            if (chars[i] == '0' && chars[i + 1] == '1') {
                chars[i] = '1';
                chars[i + 1] = '0';
                if (count > 0) {
                    for (int j = i + 2; j < chars.length; j++) {
                        if (j < chars.length - count) {
                            chars[j] = '0';
                        } else {
                            chars[j] = '1';
                        }
                    }
                }
            }
            if (chars[i + 1] == '1') {
                count++;
            }
        }
        int m = Integer.parseInt(new String(chars), 2);
        System.out.println(m);
    }
}
