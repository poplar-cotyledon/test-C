package com.yzy.twohundred;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class RacingHorse_200_42 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] a = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int[] b = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        //a排升序
        Arrays.sort(a);
        //a全排列 + 去重
        List<List<Integer>> resultList = permute(a);
        //田忌赛马：逐一比对数组a、b，a[i]全大于b[i]者留下
        System.out.println(RacingHorse(resultList, b));
    }

    //全排列
    public static List<List<Integer>> permute(int[] nums) {
        //为了不重复使用a中元素
        boolean[] used = new boolean[nums.length];
        //路径
        LinkedList<Integer> path = new LinkedList<>();
        //全排列所有的路径
        List<List<Integer>> res = new LinkedList<>();
        //深度优先搜索函数
        dfs(nums, used, path, res);
        return res;
    }

    //深度优先搜索函数
    public static void dfs(int[] nums, boolean[] used, LinkedList<Integer> path, List<List<Integer>> res) {
        //定义递归结束规则
        if (path.size() == nums.length) {
            res.add(new LinkedList<>(path));
            return;
        }
        //遍历数字进行排列
        for (int i = 0; i < nums.length; i++) {
            //去重当前数字已经被选择的情况
            if (used[i]) {
                continue;
            }
            //去重nums中含重复数字的情况
            if (i > 0 && nums[i] == nums[i - 1] && !used[i - 1]) {
                continue;
            }

            used[i] = true;
            path.add(nums[i]);
            dfs(nums, used, path, res);
            //递归结束后逐一恢复used、path
            used[i] = false;
            path.removeLast();
        }
    }

    //返回符合的排列的个数
    private static int RacingHorse(List<List<Integer>> resultList, int[] b) {
        int count = 0;
        for (List<Integer> list : resultList) {
            boolean flag = true;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) <= b[i]) {
                    flag = false;
                    break;
                }
            }
            if(flag){
                count++;
            }
        }
        return count;
    }
}