package com.yzy.twohundred;

import java.util.*;

public class StartMultitaskSorting_200_37 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[][] relations = Arrays.stream(sc.nextLine().split(" ")).map(s -> s.split("->")).toArray(String[][]::new);

        Map<String, Integer> inDegreeMap = new HashMap<>();//存入度
        Map<String, List<String>> nextMap = new HashMap<>();//存后继结点
        for (String[] relation : relations) {
            //a依赖b，即b执行完，才能执行a
            String a = relation[0];//后执行
            String b = relation[1];//先执行
            inDegreeMap.put(b, inDegreeMap.getOrDefault(b, 0));//b的入度不变
            inDegreeMap.put(a, inDegreeMap.getOrDefault(a, 0) + 1);//a的入度+1
            nextMap.putIfAbsent(b, new ArrayList<>());
            nextMap.get(b).add(a);//b的后继节点集合添加a
            nextMap.putIfAbsent(a, new ArrayList<>());//a的后继节点集合不变
        }

        //1.收集第一层入度为0的点
        List<String> list = new ArrayList<>();
        for (String key : inDegreeMap.keySet()) {
            if (inDegreeMap.get(key) == 0) {
                list.add(key);
            }
        }
        //2.记录任务执行的顺序
        StringJoiner result = new StringJoiner(" ");

        while (list.size() > 0) {
            //如果同时有多个任务要执行，则根据任务名称字母顺序排序
            list.sort(String::compareTo);
            //收集新一层入度为0的点
            List<String> newList = new ArrayList<>();

            for (String key : list) {
                //计入最终结果
                result.add(key);
                //遍历后继结点们
                List<String> nextNodes = nextMap.get(key);
                for (String nextNode : nextNodes) {
                    inDegreeMap.put(nextNode, inDegreeMap.get(nextNode) - 1);
                    if (inDegreeMap.get(nextNode) == 0) {
                        newList.add(nextNode);
                    }
                }
            }
            list = newList;
        }
        System.out.println(result);
    }
}
