package com.yzy.twohundred;

import java.util.Scanner;

public class GetShortestPathBetween2Strings_200_26 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] strings = sc.nextLine().split(" ");
        String col = strings[0];//列
        String row = strings[1];//行

        //初始时preRow记录第1行上各点到(0,0)点的最短距离，即为(0,0) -> (0,j)的直线路径
        int[] preRow = new int[col.length() + 1];
        for (int i = 0; i <= col.length(); i++) {
            preRow[i] = i;
        }

        //初始时curRow记录第2行上各点到(0,0)点的最短距离
        int[] curRow = new int[col.length() + 1];
        for (int i = 1; i <= row.length(); i++) {
            //curRow[0]是指(i,0)点到(0,0)点的最短距离，即为(0,0) -> (i,0)的直线路径
            curRow[0] = i;
            for (int j = 1; j <= col.length(); j++) {
                if (col.charAt(j - 1) == row.charAt(i - 1)) {
                    // 如果可以走斜线，则选走斜线的点
                    curRow[j] = preRow[j - 1] + 1;
                } else {
                    // 如果不能走斜线，则从当前点的上方点、左方点中选择一个较小值
                    curRow[j] = Math.min(preRow[j], curRow[j - 1]) + 1;
                }
            }
            //压缩：将curRow复制给preRow，记录第2行上各点到(0,0)点的最短距离，curRow则新记录第3行上各点到(0,0)点的最短距离
            System.arraycopy(curRow, 0, preRow, 0, col.length() + 1);
        }
        System.out.println(curRow[col.length()]);
    }
}
