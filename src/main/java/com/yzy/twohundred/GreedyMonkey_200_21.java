package com.yzy.twohundred;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GreedyMonkey_200_21 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int totalNum = sc.nextInt();
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < totalNum; i++) {
            list.add(sc.nextInt());
        }
        int n = sc.nextInt();

        //初始时，左边选0个
        //初始时，右边选n个
        int sum = 0;
        for (int i = totalNum - n; i < totalNum; i++) {
            sum += list.get(i);
        }

        //如果全选，则输出sum即可
        if (n == totalNum) {
            System.out.println(sum);
            return;
        }
        //如果非全选
        int max = sum;
        int left = 0;
        int right = totalNum - n;
        while (left < n) {
            sum += list.get(left++) - list.get(right--);
            max = Math.max(max, sum);
        }
        System.out.println(max);
    }
}
