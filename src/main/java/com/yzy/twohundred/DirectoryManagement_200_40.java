package com.yzy.twohundred;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DirectoryManagement_200_40 {
    //定义树结点
    static class TreeNode {
        String curDicName;//当前目录名称
        TreeNode father;//父目录
        Map<String, TreeNode> children;//子目录们

        //含参构造器
        public TreeNode(String curDicName, TreeNode father) {
            this.curDicName = curDicName;
            this.father = father;
            this.children = new HashMap<>();
        }
    }

    //定义树
    static class Tree {
        TreeNode root;//根目录
        TreeNode cur;//当前所在目录

        //无参构造器
        public Tree() {
            //root是根目录，根目录 / 作为初始目录
            this.root = new TreeNode("/", null);
            //cur用于指向当前正在操作的目录
            this.cur = root;
        }

        //1.创建日录
        public void mkdir(String childDicName) {
            //mkdir 目录名称，如 mkdir abc 为在当前目录创建abc目录，如果已存在同名目录则不执行任何操作
            this.cur.children.putIfAbsent(childDicName, new TreeNode(childDicName + "/", this.cur)); // 目录符号为 /
        }

        //2.进入目录
        public void cd(String dicName) {
            if (dicName.equals("..")) {
                //① cd .. 为返回上级目录，如果目录不存在则不执行任何操作
                if (this.cur.father != null) {
                    //cur 变更指向上级目录
                    this.cur = this.cur.father;
                }
            } else {
                //② cd 目录名称，如 cd abc 为进入abc目录，如果目录不存在则不执行任何操作
                if (this.cur.children.containsKey(dicName)) {
                    // cur 变更指向下级目录
                    this.cur = this.cur.children.get(dicName);
                }
            }
        }

        //3.查看当前所在路径
        public String pwd() {
            StringBuilder sb = new StringBuilder();
            //倒序路径，即不停向上找父目录
            TreeNode cur = this.cur;
            while (cur != null) {
                // 头插目录名，保证路径中目录层级正确
                sb.insert(0, cur.curDicName);
                cur = cur.father;
            }
            return sb.toString();
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //初始化目录结构
        Tree tree = new Tree();
        //记录最后一条命令的输出
        String lastCommandOutPut = "/";

        OUT:
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            //本地测试解开此行
            if (line.equals("")){
                break;
            }
            String[] tmp = line.split(" ");
            String cmd_key = tmp[0];//命令

            //约束
            if (cmd_key.equals("pwd")) {
                //约束①：pwd命令不需要参数
                if (tmp.length != 1) {
                    continue;
                }
                lastCommandOutPut = tree.pwd();
            } else if (cmd_key.equals("mkdir") || cmd_key.equals("cd")) {
                //约束②：mkdir和cd命令的参数仅支持单个目录，如：mkdir abc 和 cd abc
                if (tmp.length != 2) {
                    continue;
                }
                String cmd_val = tmp[1];//目录名
                //约束③：mkdir和cd命令（排除cd ..）的目录名称仅支持小写字母
                //约束④：不支持嵌套路径和绝对路径，如 mkdir abc/efg，cd abc/efg，mkdir /abc/efg，cd /abc/efg 是不支持的。
                //关于嵌套路径和绝对路径，我简单理解就是cmd_val含有'/'字符，可以被小写字母判断涵盖住
                if (!(cmd_key.equals("cd") && cmd_val.equals(".."))) {
                    for (int i = 0; i < cmd_val.length(); i++) {
                        char c = cmd_val.charAt(i);
                        if (c < 'a' || c > 'z') {
                            continue OUT;
                        }
                    }
                }

                //执行命令
                if (cmd_key.equals("mkdir")) {
                    tree.mkdir(cmd_val);
                } else {
                    tree.cd(cmd_val);
                }
                lastCommandOutPut = "/";
            }
        }
        System.out.println(lastCommandOutPut);
    }
}
