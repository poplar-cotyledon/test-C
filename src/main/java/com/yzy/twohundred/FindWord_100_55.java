package com.yzy.twohundred;

import java.util.*;

public class FindWord_100_55 {
    static int n;
    static String[][] matrix;
    static String word;
    static boolean[][] used;
    static LinkedList<String> path = new LinkedList<>();
    //右、下、左、上
    static int[][] offsets = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        n = Integer.parseInt(sc.nextLine());
        matrix = new String[n][n];
        for (int i = 0; i < n; i++) {
            matrix[i] = sc.nextLine().split(",");
        }
        word = sc.nextLine();
        used = new boolean[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (dfs(i, j, 0)) {
                    StringJoiner sj = new StringJoiner(",");
                    for (String loc : path) {
                        sj.add(loc);
                    }
                    System.out.println(sj);
                    return;
                }

            }
        }
        System.out.println("N");
    }

    public static boolean dfs(int i, int j, int k) {
        if (i < 0 || i >= n || j < 0 || j >= n || !word.substring(k, k + 1).equals(matrix[i][j]) || used[i][j]) {
            return false;
        }
        path.add(i + "," + j);
        used[i][j] = true;
        //结束条件：
        if (path.size() == word.length()) {
            return true;
        }
        //往上、下、左、右方向探索
        boolean result = dfs(i - 1, j, k + 1) || dfs(i + 1, j, k + 1) || dfs(i, j - 1, k + 1) || dfs(i, j + 1, k + 1);
        //回溯
        if (!result) {
            used[i][j] = false;
            path.removeLast();
        }
        return result;
    }
}
