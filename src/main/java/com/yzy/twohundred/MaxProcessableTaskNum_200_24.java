package com.yzy.twohundred;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;

public class MaxProcessableTaskNum_200_24 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] ranges = new int[n][2];
        for (int i = 0; i < n; i++) {
            ranges[i][0] = sc.nextInt();//开始时间
            ranges[i][1] = sc.nextInt();//结束时间
        }
        //任务数组中，将所有任务按照“结束时间”降序
        Arrays.sort(ranges, (a, b) -> b[1] - a[1]);
        //优先队列中，记录的是任务的“开始时间”，降序，即开始时间越大，优先级越高
        PriorityQueue<Integer> pq = new PriorityQueue<>((a, b) -> b - a);
        //记录优先队列中任务的相同结束时间
        int pq_end = Integer.MAX_VALUE;
        //最大任务数
        int count = 0;
        for (int[] range : ranges) {
            int start = range[0];
            int end = range[1];
            //1.当 pq非空 且 end<pq_end 时
            while (pq.size() > 0 && end < pq_end) {
                int pq_start = pq.poll();
                if (pq_start <= pq_end) {
                    count++;
                    pq_end--;//一个时刻只执行一个任务
                }
            }
            //2.当 pq为空 或 end=pq_end 时
            pq.add(start);
            pq_end = end;
        }
        //收尾处理：即遍历完整个ranges，但是pq中还有任务
        while (pq.size() > 0) {
            int pq_start = pq.poll();
            if (pq_start <= pq_end) {
                count++;
                pq_end--;
            }
        }
        System.out.println(count);
    }
}
