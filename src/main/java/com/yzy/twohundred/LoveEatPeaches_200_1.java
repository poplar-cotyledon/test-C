package com.yzy.twohundred;

import java.util.*;

public class LoveEatPeaches_200_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s;
        int hour;
        try {
            s = sc.nextLine();
            hour = Integer.parseInt(sc.nextLine());
        } catch (NumberFormatException e) {
            System.out.println(0);
            return;
        }
        if (s.isEmpty()) {
            System.out.println(0);
            return;
        }

        int[] tree = Arrays.stream(s.split(" ")).mapToInt(Integer::parseInt).toArray();
        if (tree == null || tree.length <= 0 || tree.length >= 10000 ||
                hour <= 0 || hour >= 10000 ||
                tree.length > hour) {
            System.out.println(0);
            return;
        }

        int sum = 0;
        List<Integer> treeList = new ArrayList<>();
        for (int t : tree) {
            if (t != 0) {
                sum += t;
                treeList.add(t);
            }
        }
        treeList.sort((o1, o2) -> o2 - o1);//降序
        //树棵树==无守卫时间
        if (treeList.size() == hour) {
            System.out.println(treeList.get(0));
            return;
        }
        //树棵树<无守卫时间
        int start = sum / hour;//最小速度 15
        int end = treeList.get(0);//最大速度 30
        OUT:
        for (; start <= end; start++) {
            int count = 0;//计数器
            for (Integer t : treeList) {
                count += t / start + ((t % start == 0 ? 0 : 1));
                if (count > hour) {
                    continue OUT;
                }
            }
            System.out.println(start);
            return;
        }
        System.out.println(0);
    }
}
