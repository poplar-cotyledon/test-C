package com.yzy.twohundred;

import java.util.Scanner;

public class TransportationTime_200_44 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int carNum = sc.nextInt();
        int distance = sc.nextInt();

        //到达时刻
        //① 后车比前车快，则按前车到达时间算
        //② 后车比前车慢，则按后车到达时间算
        double arrivedTime = 0;
        for (int i = 0; i < carNum; i++) {
            double speed = sc.nextDouble();//后车的速度
            double time = distance / speed + i;//后车到达时刻 = 路上花费的时间 + 出发时刻
            arrivedTime = Math.max(arrivedTime, time);
        }
        //最后1辆车路上花费的时间 = 最后1辆车的到达时刻 - 最后1辆车的出发时刻
        double cost = arrivedTime - (carNum - 1);
        System.out.println(cost);
    }
}
