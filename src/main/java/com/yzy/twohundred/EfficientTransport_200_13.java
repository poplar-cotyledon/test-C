package com.yzy.twohundred;

import java.util.Scanner;

public class EfficientTransport_200_13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int wa = sc.nextInt();
        int wb = sc.nextInt();
        int wt = sc.nextInt();
        int pa = sc.nextInt();
        int pb = sc.nextInt();
        //记录最大利润
        int result = 0;
        //装入货车的A货物数量至少为1，至多为(wt-1*wb)/wa，即B货物数量为1，其余都是A
        //从min到max枚举装入货车的A货物数量
        for (int A = 1; A <= (wt - wb) / wa; A++) {
            int remain = wt - wa * A;//装完A货物后，剩余重量
            if (remain % wb == 0) {
                int B = remain / wb;
                result = Math.max(result, A * pa + B * pb);
            }
        }
        System.out.println(result);
    }
}
