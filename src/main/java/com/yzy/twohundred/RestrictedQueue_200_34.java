package com.yzy.twohundred;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringJoiner;

public class RestrictedQueue_200_34 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] ins = Arrays.stream(sc.nextLine().split(",")).mapToInt(Integer::parseInt).toArray();
        int[] outs = Arrays.stream(sc.nextLine().split(",")).mapToInt(Integer::parseInt).toArray();
        if(outs.length> ins.length){
            System.out.println("NO");
            return;
        }

        LinkedList<Integer> list = new LinkedList<>();
        StringJoiner sj = new StringJoiner("");
        int i = 0;
        int j = 0;
        while (i < ins.length) {
            int in = ins[i];
            int out = outs[j];
            if (in != out) {
                if (list.contains(out)) {
                    if (list.get(0) == out) {
                        //左头
                        sj.add("L");
                        list.removeFirst();
                        j++;
                    } else if (list.getLast() == out) {
                        //右尾
                        sj.add("R");
                        list.removeLast();
                        j++;
                    } else {
                        System.out.println("NO");
                        return;
                    }
                } else {
                    list.addLast(in);
                    i++;
                }
            } else {
                sj.add("R");
                i++;
                j++;
            }
        }

        if (!list.isEmpty()) {
            while (j < outs.length) {
                int out = outs[j];
                if (list.contains(out)) {
                    if (list.getFirst() == out) {
                        //左头
                        sj.add("L");//可以保证最后一个先从左出
                        list.removeFirst();
                        j++;
                    } else if (list.getLast() == out) {
                        //右尾
                        sj.add("R");
                        list.removeLast();
                        j++;
                    } else {
                        System.out.println("NO");
                        return;
                    }
                } else {
                    System.out.println("NO");
                    return;
                }
            }
        }
        System.out.println(sj);
    }
}
