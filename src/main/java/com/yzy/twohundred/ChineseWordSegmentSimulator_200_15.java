package com.yzy.twohundred;

import java.util.*;

public class ChineseWordSegmentSimulator_200_15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        LinkedList<String> sentenceQueue = new LinkedList<>(Arrays.asList(sc.nextLine().split("[,.;]")));
        Set<String> dictionarySet = new HashSet<>(Arrays.asList(sc.nextLine().split("[,.;]")));
        LinkedList<String> result = new LinkedList<>();

        while (sentenceQueue.size() > 0) {
            String sentence = sentenceQueue.removeFirst();
            int len = sentence.length();//10
            for (; len > 0; len--) {
                //截取句子[0,len)范围子串词汇, 这样的就能实现优先最长匹配
                //并且由于是必须从0索引开始截取，因此满足了分词顺序优先
                String substrings = sentence.substring(0, len);
                if (dictionarySet.contains(substrings)) {
                    result.add(substrings);
                    //我理解词库中每个词汇只能使用一次
                    dictionarySet.remove(substrings);
                    //若子串词汇只是句子的一部分，则句子剩余部分还要继续去词库中查找
                    if (len < sentence.length()) {
                        sentenceQueue.addFirst(sentence.substring(len));
                    }
                    break;
                }
            }
            //没有在词库中找到对应子串词汇，则输出单个字母
            if (len == 0) {
                //注意，这里只放一个字母到结果中，句子剩余部分继续去词库中查找
                result.add(sentence.charAt(0) + "");
                if (sentence.length() > 1) {
                    sentenceQueue.addFirst(sentence.substring(1));
                }
            }
        }

        StringJoiner sj = new StringJoiner(",");
        result.forEach(sj::add);
        System.out.println(sj);
    }
}
