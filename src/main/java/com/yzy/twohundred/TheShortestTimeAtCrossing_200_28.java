package com.yzy.twohundred;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

//本题实际考试为核心代码模式，即只需要写出下面Solution类实现即可
class Solution {
    //上、右、下、左
    static int[][] offsets = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

    /**
     * @param lights n*m个街口每个交通灯的周期，值范围[0，120]，n和m的范围为[1,9]
     * @param timePreRoad 相邻两个街口之间街道的通行时间，范围为[0,600]
     * @param rowStart 起点的行号
     * @param colStart 起点的列号
     * @param rowEnd 终点的行号
     * @param colEnd 终点的列号
     * @return lights[rowStart][colStart]与 lights[rowEnd][colEnd]两个街口之间的最短通行时间
     */
    int calcTime(int[][] lights, int timePreRoad, int rowStart, int colStart, int rowEnd, int colEnd) {
        int n = lights.length;//行数
        int m = lights[0].length;//列数
        //1.初始化
        //最短路表：dist[i][j][k]表示以来源方向k到达位置(i,j)的最短路权重（所需要的时间），初始为Integer.MAX_VALUE
        int[][][] dist = new int[n][m][4];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                //到达位置(i,j)的路径有四个来源方向
                for (int k = 0; k < 4; k++) {
                    dist[i][j][k] = Integer.MAX_VALUE;
                }
            }
        }
        //2.出发点
        //小顶堆，堆中元素是数组 [前一个位置行号，前一个位置列号，当前位置行号，当前位置列号，到达当前位置需要的时间]
        //到达当前位置的时间越小，优先级越高
        PriorityQueue<int[]> pq = new PriorityQueue<>(Comparator.comparingInt(a -> a[4]));
        //四个来源方向到达出发点位置 (rowStart, colStart) 所需时间均为 0
        for (int k = 0; k < 4; k++) {
            dist[rowStart][colStart][k] = 0;
            //出发点位置没有前一个位置，因此前一个位置设为(-1,-1)
            pq.add(new int[]{-1, -1, rowStart, colStart, 0});
        }
        //3.加入了贪心思维的BFS——使用优先队列
        while (pq.size() > 0) {
            //①每次取出最短路，首次取的必定是出发点
            int[] tmp = pq.poll();
            int preX = tmp[0];//前一个位置行号
            int preY = tmp[1];//前一个位置列号
            int curX = tmp[2];//当前位置行号
            int curY = tmp[3];//当前位置列号
            int cost = tmp[4];//到达当前位置需要的时间
            //②向上、右、下、左四个方向探索
            for (int k = 0; k < 4; k++) {
                //新位置
                int newX = curX + offsets[k][0];
                int newY = curY + offsets[k][1];
                //新位置越界，则不可进入
                if (newX < 0 || newX >= n || newY < 0 || newY >= m){
                    continue;
                }
                //本题不允许掉头，因此新位置处于掉头位置的话，不可进入
                if (newX == preX && newY == preY){
                    continue;
                }
                //每走一步都要花费 timePerRoad 单位时间
                int newCost = cost + timePreRoad;
                //出发的第一步，或者右拐，不需要等待红绿灯，其他情况需要等待红绿灯 lights[curX][curY] 单位时间
                if (preX != -1 && preY != -1 && getDirection(preX, preY, curX, curY, newX, newY) >= 0) {
                    newCost += lights[curX][curY];
                }
                //如果以来源方向k到达位置（newX, newY）花费的时间 newCost 并非更优，则终止对应路径探索
                if (newCost >= dist[newX][newY][k]) {
                    continue;
                }
                //否则更新为更优时间
                dist[newX][newY][k] = newCost;
                //并继续探索该路径
                pq.add(new int[]{curX, curY, newX, newY, newCost});
            }
        }
        //最终取(rowEnd, colEnd)终点位置的四个来源方向路径中最短时间的作为题解
        int result = Integer.MAX_VALUE;
        for (int k = 0; k < 4; k++) {
            result = Math.min(result, dist[rowEnd][colEnd][k]);
        }
        return result;
    }

    /**
     * 根据三点坐标，确定拐弯方向
     *
     * @param preX  前一个点横坐标
     * @param preY  前一个点纵坐标
     * @param curX  当前点横坐标
     * @param curY  当前点纵坐标
     * @param nextX 下一个点横坐标
     * @param nextY 下一个点纵坐标
     * @return cur到next的拐弯方向， >0 表示向左拐， ==0 表示直行（含调头）， <0 表示向右拐
     */
    public static int getDirection(int preX, int preY, int curX, int curY, int nextX, int nextY) {
        //向量A:pre->cur
        int x1 = curX - preX;
        int y1 = curY - preY;
        //向量B:cur->next
        int x2 = nextX - curX;
        int y2 = nextY - curY;
        //两个向量的叉积A*B=x1*y2-x2*y1=|A||B|sina，其中 >0 表示向左拐， ==0 表示直行（含调头）， <0 表示向右拐
        return x1 * y2 - x2 * y1;
    }
}

//输入处理类，本题实际考试为核心代码模式，无需进行输入处理
public class TheShortestTimeAtCrossing_200_28 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//行
        int m = sc.nextInt();//列
        int[][] lights = new int[n][m];//n*m个街口每个交通灯的周期
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                lights[i][j] = sc.nextInt();
            }
        }
        int timePreRoad = sc.nextInt();//相邻两个街口之间街道的通行时间
        int rowStart = sc.nextInt();//起点的行号
        int colStart = sc.nextInt();//起点的列号
        int rowEnd = sc.nextInt();//终点的行号
        int colEnd = sc.nextInt();//终点的列号
        System.out.println(new Solution().calcTime(lights, timePreRoad, rowStart, colStart, rowEnd, colEnd));
    }
}
