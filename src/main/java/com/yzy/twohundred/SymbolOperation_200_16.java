package com.yzy.twohundred;

import java.util.*;

public class SymbolOperation_200_16 {
    // 分数
    static class Fractions {
        int ch; // 分子
        int fa; // 分母

        public Fractions() {
        }

        public Fractions(int ch, int fa) {
            this.fa = fa;
            this.ch = ch;
        }
    }

    // 操作数栈
    static Stack<Fractions> numStack = new Stack<>();
    // 操作符栈
    static Stack<Character> signStack = new Stack<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(getResult(sc.nextLine()));
    }

    public static String getResult(String s) {
        // +,-,*,/ 运算符优先级
        Map<Character, Integer> priority = new HashMap<>();
        priority.put('+', 1);
        priority.put('-', 1);
        priority.put('*', 2);
        priority.put('/', 2);
        // 操作数的字符缓存容器
        StringBuilder sb = new StringBuilder();

        // 便于最后一轮计算
        s += "+0";
        // 扫描表达式
        for (char c : s.toCharArray()) {
            if (c == ' ') {
                continue;
            }
            // 一、扫描到操作数
            if (c >= '0' && c <= '9') {
                sb.append(c);
                continue;
            }
            if (!sb.isEmpty()) {
                // 扫描结束，将操作数缓存容器中记录的字符，变为分数后，压入操作数栈
                numStack.push(new Fractions(Integer.parseInt(sb.toString()), 1));
                // 清空操作数缓存容器
                sb = new StringBuilder();
            }

            // 二、扫描到操作符
            if (c == ')') {
                // 1.当前运算符为')'
                // ①遇到')', 需要将操作符栈中靠近栈顶的'('后面的运算都出栈做了
                while (signStack.peek() != '(') {
                    calc();
                }
                // ②最后将')'对应的'('出栈
                signStack.pop();
            } else if (signStack.empty() || c == '(' || signStack.peek() == '(' || priority.get(c) > priority.get(signStack.peek())) {
                // 2.当前运算符不为')' && (运算符栈为空 || 当前运算符为'(' || 栈顶为'(' || 当前运算符的优先级 > 栈顶运算符的优先级)，直接入栈
                signStack.push(c);
            } else {
                // 3.当前运算符的优先级<=栈顶运算符的优先级
                while (!signStack.isEmpty() && signStack.peek() != '(' && priority.get(c) <= priority.get(signStack.peek())) {
                    calc();
                }
                signStack.push(c);
            }
        }
        // numStack栈中只剩一个数时，该数就是表达式结果
        Fractions result = numStack.pop();
        // 如果结果的分母为0（除数为0），则不合法
        if (result.fa == 0) {
            return "ERROR";
        }
        // 如果结果的分子为0，且分母不为0，则直接返回0
        if (result.ch == 0) {
            return "0";
        }

        // 求分子、分母的最大公约数，并进行约份，求得最简格式的分子，分母
        int k = getMaxCommonDivisor(result.ch, result.fa);
        result.ch /= k;
        result.fa /= k;
        // 求计算结果的符号，这里用乘法是为了避免 分母小，分子大，除法结果为0的情况，这样会丢失符号信息
        String sign = result.fa * result.ch < 0 ? "-" : "";
        int ch = Math.abs(result.ch);
        int fa = Math.abs(result.fa);
        if (fa == 1) {
            // 如果分母为1，则直接输出分子
            return sign + ch;
        } else {
            // 如果分母不为1，则输出 分子 / 分母
            return sign + ch + "/" + fa;
        }
    }

    // 取出numStack栈顶两个操作数进行运算
    public static void calc() {
        // 操作数顺序会对运算产生影响
        Fractions b = numStack.pop(); // 栈顶元素是运算符右边的操作数
        Fractions a = numStack.pop(); // 栈顶倒数第二个元素是运算符左边的操作数
        // 运算符
        char op = signStack.pop();
        // 记录运算结果
        Fractions result = new Fractions();
        // 开始运算
        switch (op) {
            case '+' -> {
                result.ch = a.ch * b.fa + b.ch * a.fa;
                result.fa = a.fa * b.fa;
            }
            case '-' -> {
                result.ch = a.ch * b.fa - b.ch * a.fa;
                result.fa = a.fa * b.fa;
            }
            case '*' -> {
                result.ch = a.ch * b.ch;
                result.fa = a.fa * b.fa;
            }
            case '/' -> {
                result.ch = a.ch * b.fa;
                result.fa = a.fa * b.ch;
            }
        }
        numStack.add(result);
    }

    // 辗转相除法，求两个数的最大公约数
    public static int getMaxCommonDivisor(int x, int y) {
        while (x != 0) {
            int tmp = x;
            x = y % x;
            y = tmp;
        }
        return y;
    }
}
