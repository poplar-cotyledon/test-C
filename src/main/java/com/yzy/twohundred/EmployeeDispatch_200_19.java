package com.yzy.twohundred;

import java.util.Scanner;

public class EmployeeDispatch_200_19 {
    static long x;//代号为x的国家
    static long y;//代号为y的国家
    static long cntX;//代号为x的国家需要cntX名员工
    static long cntY;//代号为y的国家需要cntY名员工

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        x = sc.nextInt();
        y = sc.nextInt();
        cntX = sc.nextInt();
        cntY = sc.nextInt();

        // 使用此范围，实际通过率55%
        //  long min = cntX + cntY;
        //  long max = Long.MAX_VALUE;
        //  long max = 1000000000L;

        // 使用此范围，实际通过率可以100%
        long min = 1;
        long max = (long) 1e9;

        while (min <= max) {
            long mid = min + (max - min) / 2;//避免超出long型范围
            long check = check(mid);
            if (check < 0) {
                //k太大了，够分配，缩小右边界
                max = mid - 1;
            } else if (check > 0) {
                //k太小了，不够分配，扩大左边界
                min = mid + 1;
            } else {
                //k刚刚好，去掉x与y的公倍数
                while (mid % (x * y) == 0) {
                    mid--;
                }
                System.out.println(mid);
                return;
            }
        }
        System.out.println(min);
    }

    public static long check(long k) {
        long A = k / x; // 1~k范围内x倍数的数量
        long B = k / y; // 1~k范围内y倍数的数量
        long C = k / (x * y); // 1~k范围内x*y倍数的数量
        long D = k - A - B + C;//1~k范围内非x倍数也非y倍数的数
        long giveX = B - C;//给x国
        long giveY = A - C;//给y国
        long needX = Math.max(0, cntX - giveX);//x国还差
        long needY = Math.max(0, cntY - giveY);//y国还差
        return needX - D + needY;//避免超出long型范围,实际为：needX + needY - D
    }
}