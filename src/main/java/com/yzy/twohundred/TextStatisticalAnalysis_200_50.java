package com.yzy.twohundred;

import java.util.Scanner;

public class TextStatisticalAnalysis_200_50 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        StringBuilder sb = new StringBuilder();
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            // 本地测试时，以输入空行作为结束条件，实际考试时无需此逻辑
            if (line.length() == 0) {
                break;
            }
            line = line.replaceAll("\\\\[\"']", "a") // 替换\"和\'为普通字符
                    .replaceAll("\".*?\"", "a") // 将成对双引号及其中内容替换为普通串
                    .replaceAll("'.*?'", "a") // 将成对单引号及其中内容替换为普通串
                    .replaceAll("--.+", "") // 将--及其往后的注释替换为空串
                    .replaceAll("\\s+", "") // 将空白字符替换为空串
                    .replaceAll(";+", ";"); // 将连续分号替换为单个分号
            sb.append(line);
        }
        // 为了避免复杂处理，这里无论最后一条文本有没有分号，都加一个
        sb.append(";");
        String s = sb.toString().replaceAll(";+", ";")// 将连续分号替换为单个分号
                .replaceAll("^;", "");// 去掉行首分号
        System.out.println(sb);
        // 记录文本条数
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ';') {
                count++;
            }
        }
        System.out.println(count);
    }
}