package com.yzy.twohundred;

import java.util.Scanner;

public class DivideMoonCake_200_5 {
    static int m;//员工数
    static int n;//月饼数
    static int D_value = 3;//差值
    static int result = 0;//方案数
//    static LinkedList<Integer> path = new LinkedList<>();//记录分法

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        m = sc.nextInt();
        n = sc.nextInt();
        if (m == 1) {
            //如果只有1个员工，那么只有1种方案
            System.out.println(1);
        } else {
            //如果超过1个员工，为保证方案不重复，必须保证第i个员工的月饼数量≤第i+1个员工的月饼数量
            //因此，对于第0个员工，至少分得1个月饼，至多分得n/m个月饼（均分数量）
            recursion(1, 1, n / m, n);
            System.out.println(result);
        }
    }

    /**
     * 分治递归
     *
     * @param level  第几个员工
     * @param min    至少分得几个月饼
     * @param max    至多分得几个月饼
     * @param remain 还剩几个月饼
     */
    private static void recursion(int level, int min, int max, int remain) {
        //结束条件：
        //  分到最后一个员工时，剩下所有月饼remain都给他，而他的前一个员工分得月饼min个
        //  如果二者差距≤差值D_value(3)，方案成立
        if (level == m) {
            if (remain - min <= D_value) {
                result++;
//                path.add(remain);
//                System.out.println(path);
//                path.removeLast();
            }
            return;
        }

        //循环条件：
        //  当前员工至少分得min个月饼，至多分得max个月饼
        for (int i = min; i <= max; i++) {
            remain -= i;
//            path.add(i);
            //下一个员工至少分得i个月饼，至多分得Math.min((i+D_value), remain/(m-level))个月饼
            //              最小值                   最大值       平均值
            recursion(level + 1, i, Math.min((i + D_value), remain / (m - level)), remain);
            //回溯
            remain += i;
//            path.removeLast();
        }
    }
}
