package com.yzy.twohundred;

import java.util.*;

public class MaxSocialDistance_200_49 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int seatNum = Integer.parseInt(sc.nextLine());
        String s = sc.nextLine();
        int[] arr = Arrays.stream(s.substring(1, s.length() - 1).split(",")).mapToInt(Integer::parseInt).toArray();

        LinkedList<String> seatIdx = new LinkedList<>();//记录已经坐人位置的序号
        int lastSeatIdx = -1;//记录题解
        for (int i : arr) {
            //1.如果当前元素值为负数，表示出场（特殊：位置 0 的员工不会离开）
            if (i < 0 && seatIdx.contains(-i + "")) {
                seatIdx.remove(-i + "");
                continue;
            }
            //2.如果当前元素值为 1，表示进场
            //① 没有空闲位置，则坐不下 ？？
            if (seatIdx.size() == seatNum) {
                lastSeatIdx = -1;
                continue;
            }
            if (seatIdx.size() == 0) {
                //② 没有人时，坐第一个位置
                seatIdx.add(0 + "");
                lastSeatIdx = 0;
            } else if (seatIdx.size() == 1) {
                //③ 只有一个人时，坐最后一个位置，因为位置 0 的员工不会离开
                seatIdx.add((seatNum - 1) + "");
                lastSeatIdx = seatNum - 1;
            } else {
                //④ 不止一个人时
                int bestSeatDis = -1;//记录最大社交距离
                int bestSeatIdx = -1;//记录具有最大社交距离的座位号

                //找到连续空闲座位区域（该区域左、右边界是坐了人的座位）
                int l = 0;
                int r = 1;
                while (r < seatIdx.size()) {
                    int left = Integer.parseInt(seatIdx.get(l));//左边界
                    int right = Integer.parseInt(seatIdx.get(r));//右边界
                    int dis = right - left - 1;
                    if (dis > 0) {
                        int curSeatDis = dis / 2 - (dis % 2 == 0 ? 1 : 0);
                        int curSeatIdx = left + curSeatDis + 1;
                        //保留最优解
                        if (curSeatDis > bestSeatDis) {
                            bestSeatDis = curSeatDis;
                            bestSeatIdx = curSeatIdx;
                        }
                    }
                    l++;
                    r++;
                }
                //如果最后一个座位没有坐人的话，需要特殊处理
                if (!seatIdx.contains((seatNum - 1) + "")) {
                    // 此时可以直接坐到第 seatNum - 1 号座位，最大社交距离为 curSeatDis
                    int curSeatDis = seatNum - 1 - Integer.parseInt(seatIdx.getLast()) - 1;
                    int curSeatIdx = seatNum - 1;
                    // 保留最优解
                    if (curSeatDis > bestSeatDis) {
                        bestSeatIdx = curSeatIdx;
                    }
                }

                //如果能坐人，则将坐的位置加入seatIdx中
                if (bestSeatIdx > 0) {
                    seatIdx.add(bestSeatIdx + "");
                    seatIdx.sort(Comparator.comparingInt(Integer::parseInt));//升序排已经坐人位置的序号
                }
                //假设当前人就是最后一个人，那么无论当前人是否能坐进去，都更新lastSeatIdx = bestSeatIdx
                lastSeatIdx = bestSeatIdx;
            }
        }
        System.out.println(lastSeatIdx);
    }
}
