package com.yzy.twohundred;

import java.util.*;

public class FindCityByIP_200_17 {
    static class City {
        String cityName;
        long start;
        long end;
        long len;

        public City(String cityName, String start, String end) {
            this.cityName = cityName;
            // 将IP地址转为整型
            this.start = conversion(start);
            this.end = conversion(end);
            this.len = this.end - this.start + 1;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] cities = sc.nextLine().split(";");
        List<City> cityList = new ArrayList<>();
        for (String city : cities) {
            String[] split = city.split("[=,]");
            cityList.add(new City(split[0], split[1], split[2]));
        }
        String[] targetIPs = sc.nextLine().split(",");

        StringJoiner sj = new StringJoiner(",");
        for (String targetIP : targetIPs) {
            long ip = conversion(targetIP);
            //记录该目标IP地址的最佳匹配城市
            String targetCityName = "";
            //记录最佳匹配城市IP段的长度
            long minLen = Long.MAX_VALUE;
            for (City city : cityList) {
                if (ip >= city.start && ip <= city.end) {
                    //如果带查询的IP地址，在某城市的IP段范围内，且该城市的IP段长度更小，则该城市为待查询IP的最佳匹配城市
                    //如果存在区间长度相同的匹配城市，则字典序更大的是最佳匹配城市，此类用例有20%
                    if (city.len < minLen || (city.len == minLen && targetCityName.compareTo(city.cityName) < 0)) {
                        targetCityName = city.cityName;
                        minLen = city.len;
                    }
                }
            }
            sj.add(targetCityName);
        }
        System.out.println(sj);
    }

    //IP地址转整型
    private static long conversion(String s) {
        String[] splits = s.split("\\.");
        long result = 0;
        for (String split : splits) {
            result = result * 256 + Integer.parseInt(split);
        }
        return result;
    }
}
