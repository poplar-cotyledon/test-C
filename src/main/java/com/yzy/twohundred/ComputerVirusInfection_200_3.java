package com.yzy.twohundred;

import java.util.*;

public class ComputerVirusInfection_200_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int computer_num = sc.nextInt();//电脑数
        int connect_num = sc.nextInt();//连接数
        //① 邻接表
        Map<Integer, ArrayList<int[]>> graph = new HashMap<>();
        for (int i = 0; i < connect_num; i++) {
            int from = sc.nextInt();//出发点
            int to = sc.nextInt();//目标点
            int time = sc.nextInt();//出发点到目标点的耗时
            graph.putIfAbsent(from, new ArrayList<>());
            graph.get(from).add(new int[]{to, time});
        }

        //1.初始化：假设源点不可达其他剩余点，即源点到达其他点的耗时无限大
        //② 记录最短路径长度（下标为1开始）
        int[] dist = new int[computer_num + 1];
        Arrays.fill(dist, Integer.MAX_VALUE);
        int src = sc.nextInt();//源点
        dist[src] = 0;//源点到达源点的耗时为0
        //③ 优先队列path中记录的其实是：已被探索过的路径的终点（路径指的是源点->终点）
        //  优先队列优先级规则是，路径终点的权重（即源点->终点的耗时）越小越优先
        PriorityQueue<Integer> path = new PriorityQueue<>(Comparator.comparingInt(a -> dist[a]));
        path.add(src);//初始被探索过的路径只有源点本身
        //④ 记录对应点是否在path中
        boolean[] used = new boolean[computer_num + 1];
        used[src] = true;//初始只有源点本身在needCheck中

        while (path.size() > 0) {
            //2.初始以源点作为起点，后面以取出最优路径的终点（耗时最少的路径）作为新的起点
            int from = path.poll();
            used[from] = false;
            //3.判断from是否有可达的其他点
            if (graph.containsKey(from)) {
                //4.如果有，则依次从邻接表中遍历取出
                for (int[] next : graph.get(from)) {
                    int to = next[0];//to是可达的其他店
                    int time = next[1];//time是 from->to 的耗时
                    int newDist = dist[from] + time;//newDist是 源点->to 的新耗时，旧耗时是dist[to]
                    //5.若找到更少耗时的路径，则更新
                    if (newDist < dist[to]) {
                        dist[to] = newDist;
                        //6.如果to点不在path中，则加入，因为还要以to作为新from，直到每一条分支的最后一个点
                        if (!used[to]) {
                            path.add(to);
                            used[to] = true;
                        }
                    }
                }
            }
        }
        //dist记录的是源点到达其他各点的最短耗时，我们取出其中最大的就是源点走完所有点的最短耗时
        int result = 0;
        for (int i = 1; i <= computer_num; i++) {
            result = Math.max(result, dist[i]);
        }
        //如果存在某个点无法到达，则源点到该点的耗时为Integer.MAX_VALUE
        System.out.println(result == Integer.MAX_VALUE ? -1 : result);
    }
}