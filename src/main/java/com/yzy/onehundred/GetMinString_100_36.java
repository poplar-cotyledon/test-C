package com.yzy.onehundred;

import java.util.Scanner;

public class GetMinString_100_36 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char[] chars = sc.nextLine().toCharArray();
        for (int i = 0; i < chars.length - 1; i++) {
            char min = 'z';
            int minIndex = 0;
            for (int j = i + 1; j < chars.length; j++) {
                if (chars[j] < chars[i] && chars[j] < min) {
                    min = chars[j];
                    minIndex = j;
                }
            }
            if (min != 'z') {
                char temp = chars[i];
                chars[i] = chars[minIndex];
                chars[minIndex] = temp;
                System.out.println(chars);
                return;
            }
        }
        System.out.println(chars);
    }
}
