package com.yzy.onehundred;

import java.util.*;

public class DoTasksForScore_100_26 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int taskNum = sc.nextInt();
        int time = sc.nextInt();
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < taskNum; i++) {
            int key = sc.nextInt();
            int value = sc.nextInt();
            if(!map.containsKey(key)){
                List<Integer> values = new ArrayList<>();
                values.add(value);
                map.put(key,values);
            }else {
                map.get(key).add(value);
            }
        }

        int count = 0;
        for (Integer key : map.keySet()) {
            if(key<=time){
                List<Integer> values = map.get(key);
                values.sort((o1, o2) -> o2 - o1);
                count+=values.get(0);
            }
        }
        System.out.println(count);
    }
}
