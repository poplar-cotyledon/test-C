package com.yzy.onehundred;

import java.util.*;

public class MaxNumForGems_100_27 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        List<Integer> gems = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            gems.add(sc.nextInt());
        }
        int money = sc.nextInt();

        if (num == 0) {
            System.out.println(0);
            return;
        }

        int count = 0;
        Out:
        for (int i = 0; i < gems.size() - 1; i++) {
            int j = i;
            int sum = gems.get(i);
            if (sum <= money) {
                while (j < gems.size() - 1) {
                    j++;
                    sum += gems.get(j);
                    if (sum > money) {
                        j--;
                        count = Math.max(count, j - i + 1);
                        continue Out;
                    } else if (sum == money) {
                        count = Math.max(count, j - i + 1);
                        continue Out;
                    }
                }
                count = Math.max(count, j - i + 1);
                break;
            }
        }
        System.out.println(count);
    }
}
