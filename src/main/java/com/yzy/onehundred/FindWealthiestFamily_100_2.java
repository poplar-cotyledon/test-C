package com.yzy.onehundred;

import java.util.*;

public class FindWealthiestFamily_100_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        //这里wealth长度定义为n+1，是为了让wealth数组索引对应成员编号 1~N
        long[] wealth = new long[n + 1];
        long[] family = new long[n + 1];
        for (int i = 1; i <= n; i++) {
            wealth[i] = sc.nextInt();
            family[i] = wealth[i];
        }
        for (int i = 0; i < n - 1; i++) {
            int father = sc.nextInt();
            int child = sc.nextInt();
            family[father] += wealth[child];
        }
        System.out.println(Arrays.stream(family).max().orElse(0));
    }
}
