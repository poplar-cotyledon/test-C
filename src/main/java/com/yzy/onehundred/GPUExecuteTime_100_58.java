package com.yzy.onehundred;

import java.util.Scanner;

public class GPUExecuteTime_100_58 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int perSecondMaxNum = sc.nextInt();//每秒最多能完成几个任务
        int secondNum = sc.nextInt();//有几个秒
        int perSecondTotalNum;//每个秒来的总任务数
        int result = secondNum;//完成所有任务，至少需要几秒
        int remainTask = 0;//每秒执行完<=num个任务后，剩余的任务数

        int i = 0;
        while (i < secondNum) {
            perSecondTotalNum = sc.nextInt();
            remainTask += perSecondTotalNum;
            if (remainTask <= perSecondMaxNum) {
                remainTask = 0;
            } else {
                remainTask -= perSecondMaxNum;
            }
            i++;
        }
        result += remainTask / perSecondMaxNum;
        System.out.println(remainTask % perSecondMaxNum == 0 ? result : result + 1);
    }
}