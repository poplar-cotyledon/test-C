package com.yzy.onehundred;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MaxNumTeam_100_18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            list.add(sc.nextInt());
        }
        int value = sc.nextInt();

        list.sort((o1, o2) -> o2 - o1);
        int i = 0;//头指针
        int j = num - 1;//尾指针
        int count = 0;//计数器
        while (i < j) {
            if (list.get(i) >= value) {
                count++;
                i++;
            } else {
                if (list.get(i) + list.get(j) >= value) {
                    count++;
                    i++;
                    j--;
                }else {
                    j--;
                }
            }
        }
        System.out.println(count);
    }
}
