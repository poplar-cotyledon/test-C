package com.yzy.onehundred;

import java.util.*;

public class RobotMoveBricks_100_11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] bricks = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        int sum = 0;
        List<Integer> brickList = new ArrayList<>();
        for (int brick : bricks) {
            if (brick != 0) {
                sum += brick;
                brickList.add(brick);
            }
        }
        //含货仓库大于8，8小时肯定完成不了
        if (brickList.size() > 8) {
            System.out.println(-1);
            return;
        }
        //含货仓库为空，无需完成
        if (brickList.isEmpty()) {
            System.out.println(0);
            return;
        }
        //降序
        brickList.sort((o1, o2) -> o2 - o1);
        //含货仓库等于8，能量格最小必为最大货数
        if (brickList.size() == 8) {
            System.out.println(brickList.get(0));
            return;
        }
        //含货仓库小于8
        int start = sum / 8;//最小能量格
        int end = brickList.get(0);//最大能量格
        OUT:
        for (; start <= end; start++) {
            int count = 0;//计数器
            for (Integer brick : brickList) {
                count += brick / start + ((brick % start == 0 ? 0 : 1));
                if (count > 8) {
                    continue OUT;
                }
            }
            System.out.println(start);
            return;
        }
        System.out.println(-1);
    }
}
