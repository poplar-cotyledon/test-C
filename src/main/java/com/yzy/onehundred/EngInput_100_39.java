package com.yzy.onehundred;

import java.util.*;

public class EngInput_100_39 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] words = sc.nextLine().split("[ ,'.]");
        String pre = sc.nextLine();
        int preLength = pre.length();
        char[] preChars = pre.toCharArray();

        List<String> list = new ArrayList<>();
        OUT:
        for (String word : words) {
            if (word.length() >= preLength) {
                char[] wordChars = word.toCharArray();
                for (int i = 0; i < preChars.length; i++) {
                    if (preChars[i] != wordChars[i]) {
                        continue OUT;
                    }
                }
                list.add(word);
            }
        }
        if (!list.isEmpty()) {
            list.sort(String::compareTo);
            StringBuilder sb = new StringBuilder();
            for (String s : list) {
                sb.append(s).append(" ");
            }
            sb.deleteCharAt(sb.length() - 1);
            System.out.println(sb);
        } else {
            System.out.println(pre);
        }
    }
}
