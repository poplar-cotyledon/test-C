package com.yzy.onehundred;

import java.util.*;

public class AttendanceInfo_100_55 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        StringJoiner sj = new StringJoiner(" ");
        for (int i = 0; i < n; i++) {
            String[] tokens = sc.nextLine().split(" ");
            List<String> records = new ArrayList<>(Arrays.asList(tokens));
            sj.add(isAward(records));
        }
        System.out.println(sj);
    }

    public static String isAward(List<String> records) {
        int absent = 0;//缺勤
        int present = 0;//正常上班
        String preRecord = "";//前一个记录，用于判断是否有连续的迟到/早退

        for (int i = 0; i < records.size(); i++) {
            if (i >= 7) {
                //对立事件：任意连续7次考勤，缺勤/迟到/早退<=3次才行 ====> 任意连续7次考勤，正常上班>4次才行
                //简化处理：超过7天，需要减去第i-7天的记录，若为present则present--，若为其他则不做改动
                if ("present".equals(records.get(i - 7))) {
                    present--;
                }
            }

            String curRecord = records.get(i);
            if ("absent".equals(curRecord)) {
                //① 缺勤不超过一次
                if (++absent > 1) {
                    return "false";
                }
            } else if ("late".equals(curRecord) || "leaveearly".equals(curRecord)) {
                //② 没有连续的迟到/早退
                if ("late".equals(preRecord) || "leaveearly".equals(preRecord)) {
                    return "false";
                }
            } else if ("present".equals(curRecord)) {
                present++;
            }
            preRecord = curRecord;

            //③ 任意连续7次考勤，缺勤/迟到/早退次数 = window_len - present ≤ 3次，
            int window_len = Math.min(i + 1, 7);
            if (window_len - present > 3) {
                return "false";
            }
        }
        return "true";
    }
}