package com.yzy.onehundred;

import java.util.*;

public class PasswordEntryDetect_100_5 {
    static int countUp = 0;
    static int countLow = 0;
    static int countNum = 0;
    static int countSign = 0;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char[] chars = sc.nextLine().toCharArray();
        Stack<Character> passwordStack = new Stack<>();
        for (char c : chars) {
            if (c != '<') {
                count(c, 1);
                passwordStack.push(c);
            } else {
                if (!passwordStack.isEmpty()) {
                    count(c, 0);
                    passwordStack.pop();
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        for (Character c : passwordStack) {
            sb.append(c);
        }
        if (!passwordStack.isEmpty()) {
            if (passwordStack.size() < 8 || countUp < 1 || countLow < 1 || countNum < 1 || countSign < 1) {
                System.out.println(sb + ",false");
            } else {
                System.out.println(sb + ",true");
            }
        } else {
            System.out.println(",false");
        }
    }

    public static void count(char c, int i) {
        if (i == 1) {
            if (Character.isUpperCase(c)) {
                countUp++;
            } else if (Character.isLowerCase(c)) {
                countLow++;
            } else if (Character.isDigit(c)) {
                countNum++;
            } else if (!Character.isSpaceChar(c)) {
                countSign++;
            }
        } else {
            if (Character.isUpperCase(c)) {
                countUp--;
            } else if (Character.isLowerCase(c)) {
                countLow--;
            } else if (Character.isDigit(c)) {
                countNum--;
            } else if (!Character.isSpaceChar(c)) {
                countSign--;
            }
        }
    }
}
