package com.yzy.onehundred;

import java.util.Scanner;

public class MaxLengthOfSubstrings1_100_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        char[] chars = s.toCharArray();
        int count = 0;
        for (char c : chars) {
            if (c == 'o') {
                count++;
            }
        }
        if (count % 2 == 0) {
            System.out.println(chars.length);
        } else {
            System.out.println(chars.length - 1);
        }
    }
}
