package com.yzy.onehundred;

import java.util.*;

public class GetSumOfMaxAndMin_100_68 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int[] arr = new int[m];
        for (int i = 0; i < m; i++) {
            arr[i] = sc.nextInt();
        }
        int n = sc.nextInt();
        System.out.println(getResult(arr, n));
    }

    public static int getResult(int[] arr, int n) {
        //输入非法
        if (n <= 0) {
            return -1;
        }
        //Set自带去重功能
        Set<Integer> set = new HashSet<>();
        for (int val : arr) {
            //输入非法
            if (val < 0 || val > 1000){
                return -1;
            }
            set.add(val);
        }

        //最大N个数与最小N个数不能有重叠，否则输入非法
        if (set.size() < n * 2) {
            return -1;
        }

        Integer[] distinct_arr = set.toArray(new Integer[0]);
        Arrays.sort(distinct_arr, Comparator.comparingInt(a -> a));
        int l = 0;
        int r = distinct_arr.length - 1;
        int ans = 0;
        while (n > 0) {
            ans += distinct_arr[l] + distinct_arr[r];
            l++;
            r--;
            n--;
        }
        return ans;
    }
}