package com.yzy.onehundred;

import java.util.*;

public class GetHuffmanTree_100_51 {
    //定义树的结点
    static class Node {
        //成员变量
        Node lChild;
        Node rChild;
        int weight;
        int height;
        //全参构造器
        public Node(Node lc, Node rc, int weight, int height) {
            this.lChild = lc;
            this.rChild = rc;
            this.weight = weight;
            this.height = height;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        //优先级队列，按权值升序（权值最小的在堆顶），权值相同则按层数升序
        PriorityQueue<Node> pq = new PriorityQueue<>((a, b) -> a.weight != b.weight ? a.weight - b.weight : a.height - b.height);
        //输入均为叶结点
        for (int i = 0; i < num; i++) {
            int w = sc.nextInt();
            Node node = new Node(null, null, w, 0);
            pq.add(node);
        }
        //从下网上逐步创建父结点，构成二叉树
        while (pq.size() > 1) {
            //较小值作左结点
            Node lc = pq.poll();
            //较大值作右结点
            Node rc = pq.poll();
            int fw = lc.weight + rc.weight;
            int fh = rc.height + 1;
            Node f = new Node(lc, rc, fw, fh);
            pq.add(f);
        }
        //根结点
        Node root = pq.poll();
        StringJoiner sj = new StringJoiner(" ");
        //中序遍历
        midOrder(root, sj);
        System.out.println(sj);
    }

    public static void midOrder(Node root, StringJoiner sj) {
        //左
        if (root.lChild != null) {
            midOrder(root.lChild, sj);
        }
        //根
        sj.add(String.valueOf(root.weight));
        //右
        if (root.rChild != null) {
            midOrder(root.rChild, sj);
        }
    }
}
