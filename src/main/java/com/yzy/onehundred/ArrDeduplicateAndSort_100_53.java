package com.yzy.onehundred;

import java.util.*;

public class ArrDeduplicateAndSort_100_53 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] arr = Arrays.stream(sc.nextLine().split(",")).mapToInt(Integer::parseInt).toArray();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i : arr) {
            if (!map.containsKey(i)) {
                map.put(i, 1);
            } else {
                map.put(i, map.get(i) + 1);
            }
        }
        List<Integer> list = new ArrayList<>(map.keySet());
        list.sort((o1, o2) -> map.get(o2) - map.get(o1));
        StringJoiner sj = new StringJoiner(",");
        for (Integer i : list) {
            sj.add(String.valueOf(i));
        }
        System.out.println(sj);
    }
}
