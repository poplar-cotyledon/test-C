package com.yzy.onehundred;

import java.util.Scanner;

public class WordSpell_100_16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        String[] words = new String[n];
        for (int i = 0; i < n; i++) {
            words[i] = sc.nextLine();
        }
        String chars = sc.nextLine();

        //搭建已有字符城
        char[] charsArr = new char[26];
        int anyNum = 0;//问号数量
        for (char c : chars.toCharArray()) {
            if (c == '?') {
                anyNum++;
            } else {
                charsArr[c - 'a']++;
            }
        }

        //单词匹配
        int result = 0;//匹配成功的单词个数
        for (String word : words) {
            //搭建单词字符城
            char[] wordSplit = new char[26];
            for (char c : word.toCharArray()) {
                wordSplit[c - 'a']++;
            }
            int needNum = 0;//需要问号的数量
            for (int i = 0; i < 26; i++) {
                int j = wordSplit[i] - charsArr[i];
                if (j > 0) {
                    needNum += j;//已有字符城不够，问号来凑
                }
            }
            if (needNum <= anyNum) {
                result++;//需要问号不超过已有问号，则该单词匹配成功
            }
        }
        System.out.println(result);
    }
}
