package com.yzy.onehundred;

import java.util.Arrays;
import java.util.Scanner;

public class GrayMapRecovery_100_43 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] loc = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int[] tar = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int a = tar[0] * loc[1] + tar[1] + 1;//一维坐标
        for (int i = 3; i < loc.length; i += 2) {
            if (loc[i] < a) {
                a -= loc[i];
            } else {
                System.out.println(loc[i - 1]);
                return;
            }
        }
    }
}
