package com.yzy.onehundred;

import java.util.Arrays;
import java.util.Scanner;

public class XMFindSeat_100_17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] arr = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int ming = Integer.parseInt(sc.nextLine());
        System.out.println(Math.abs(Arrays.binarySearch(arr, ming)));
    }
}
