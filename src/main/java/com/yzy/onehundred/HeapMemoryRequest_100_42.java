package com.yzy.onehundred;

import java.util.*;

public class HeapMemoryRequest_100_42 {
    static int req = 0;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Map<Integer, Integer> fullMap = new HashMap<>();

        //×【1】必须合法输入，否则失败
        try {
            req = Integer.parseInt(sc.nextLine());
            //×【2】申请内存空间非法
            if (req <= 0 || req > 100) {
                System.out.println(-1);
                return;
            }
            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                if (s.isEmpty()) {
                    break;
                }
                String[] split = s.split(" ");
                int loc = Integer.parseInt(split[0]);
                int num = Integer.parseInt(split[1]);
                //×【3】输入信息键不能有相同的，否则失败
                if (fullMap.containsKey(loc)) {
                    System.out.println(-1);
                    return;
                }
                fullMap.put(loc, num);
            }
        } catch (Exception e) {
            System.out.println(-1);
            return;
        }
        fullMap.put(-1, 1);//冗余字段"头"，用于根据full计算出empty
        fullMap.put(100, 1);//冗余字段"尾"，用于根据full计算出empty

        //1.full按"键"排升序
        List<Integer> fullKeyList = new ArrayList<>(fullMap.keySet());
        //allocationKeyList.sort(Comparator.comparingInt(allocationMap::get));
        fullKeyList.sort(Comparator.comparingInt(o -> o));
        //2.full计算得到empty
        Map<Integer, Integer> emptyMap = new HashMap<>();
        for (int i = 0; i < fullKeyList.size() - 1; i++) {
            int loc = fullKeyList.get(i) + fullMap.get(fullKeyList.get(i));
            int num = fullKeyList.get(i + 1) - loc;
            //×【4】full排升序后，前一个的“键+值” <= 后一个的“键”，否则失败
            if (num < 0) {
                System.out.println(-1);
                return;
            }
            if (num == 0) {
                continue;
            }
            emptyMap.put(loc, num);
        }

        //【5】没有可分配的内存
        if (emptyMap.isEmpty()) {
            System.out.println(-1);
            return;
        }

        //3.empty按"值"排升序，值相同则按"键"排升序
        List<Integer> emptyKeyList = new ArrayList<>(emptyMap.keySet());
        emptyKeyList.sort((o1, o2) -> {
            if (Objects.equals(emptyMap.get(o1), emptyMap.get(o2))) {
                return o1 - o2;
            }
            return emptyMap.get(o1) - emptyMap.get(o2);
        });

        //4.筛选empty中最合适req的空位
        int result = 0;
        boolean flag = false;
        for (Integer e : emptyKeyList) {
            if (req <= emptyMap.get(e)) {
                result = e;
                flag = true;
                break;
            }
        }
        if (flag) {
            System.out.println(result);
        } else {
            System.out.println(-1);
        }
    }
}