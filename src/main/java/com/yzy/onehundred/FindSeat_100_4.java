package com.yzy.onehundred;

import java.util.Scanner;

public class FindSeat_100_4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char[] chars = sc.nextLine().toCharArray();

        int count = 0;//计数器
        //遍历，第i个数为0，且前后2个数都是0，count++
        //若第1个数为0，则只需后1个数为0，即可count++
        //若最后1个数为0，则只需前1个数为0，即可count++
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '0') {
                boolean leftFlag = (i == 0) || (chars[i - 1] == '0');
                boolean rightFlag = (i == chars.length - 1) || (chars[i + 1] == '0');
                if (leftFlag && rightFlag) {
                    count++;
                    chars[i] = '1';
                    i++;//不可能相邻落座，减少循环次数
                }
            }
        }
        System.out.println(count);
    }
}