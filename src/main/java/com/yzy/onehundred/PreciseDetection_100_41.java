package com.yzy.onehundred;

import java.util.*;

public class PreciseDetection_100_41 {
    static Set<String> set = new HashSet<>();//4 6 2 3 10 9 7 8
    static Map<String, List<String>> map = new HashMap<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = Integer.parseInt(sc.nextLine());
        String confirmed = "," + sc.nextLine() + ",";
        for (int i = 0; i < num; i++) {
            String[] split = sc.nextLine().split(",");
            List<String> list = new ArrayList<>();
            for (int j = 0; j < split.length; j++) {
                if (split[j].equals("1") && j != i && !confirmed.contains("," + j + ",")) {
                    list.add("," + j + ",");
                }
            }
            map.put(("," + i + ","), list);
        }

        map.forEach((k, vs) -> { //k=1
            if (confirmed.contains(k)) {
                for (String v : vs) { //vs:4 7 8
                    set.add(v);
                    if (!map.get(v).isEmpty()) {
                        dfs(map.get(v));
                    }
                }
            }
        });
        System.out.println(set.size());
    }

    private static void dfs(List<String> vs) {
        for (String v : vs) {
            if (!set.contains(v)) {
                set.add(v);
                if (!map.get(v).isEmpty()) {
                    dfs(map.get(v));
                }
            }
        }
    }
}