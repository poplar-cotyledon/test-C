package com.yzy.onehundred;

import java.util.Scanner;

public class FindMaxGold_100_38 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int k = scanner.nextInt();

        int count = 0;
        if (n != 0 && m != 0) {
            for (int i = 0; i < m; i++) {
                int myI = i;
                if (myI > 9) {
                    char[] chars = String.valueOf(myI).toCharArray();
                    myI = 0;
                    for (char c : chars) {
                        myI += Integer.parseInt(String.valueOf(c));
                    }
                }
                if (myI > k) {
                    continue;
                }
                for (int j = 0; j < n; j++) {
                    int myJ = j;
                    if (myJ > 9) {
                        char[] chars = String.valueOf(myJ).toCharArray();
                        myJ = 0;
                        for (char c : chars) {
                            myJ += Integer.parseInt(String.valueOf(c));
                        }
                    }
                    if (myI + myJ <= k) {
                        count++;
                    }
                }
            }
        }
        System.out.println(count);
    }
}