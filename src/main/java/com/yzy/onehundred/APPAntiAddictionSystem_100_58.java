package com.yzy.onehundred;

import java.util.*;

public class APPAntiAddictionSystem_100_58 {
    static class App {
        String name;
        int priority;
        int start;
        int end;

        public App(String name, int priority, int start, int end) {
            this.name = name;
            this.priority = priority;
            this.start = start;
            this.end = end;
        }
    }

    //将时间变为分钟值
    public static int convert(String time) {
        String[] parts = time.split(":");
        int hours = Integer.parseInt(parts[0]);
        int minutes = Integer.parseInt(parts[1]);
        return hours * 60 + minutes;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        List<App> requireList = new ArrayList<>();//请求App列表
        for (int i = 0; i < n; i++) {
            String[] input = sc.nextLine().split(" ");
            requireList.add(new App(input[0], Integer.parseInt(input[1]), convert(input[2]), convert(input[3])));
        }
        int queryTime = convert(sc.nextLine());
        System.out.println(getResult(requireList, queryTime));
    }

    public static String getResult(List<App> requireList, int queryTime) {
        List<App> loginList = new ArrayList<>();//注册上的App列表
        for (App requireApp : requireList) { //请求App
            //起始时间需小于结束时间，否则跳过
            if (requireApp.start >= requireApp.end) {
                continue;
            }
            List<Integer> deleteList = new ArrayList<>();//存放loginList中优先级低于requireApp的元素
            boolean flag = false;
            for (int j = 0; j < loginList.size(); j++) {
                App loginApp = loginList.get(j);//注册上的App
                //1.”请求App“和”已注册App“的请求时间是否有交叉
                if (loginApp.start >= requireApp.end || requireApp.start >= loginApp.end) {
                    //①无交叉，则继续循环
                    continue;
                }
                //②有交叉
                //2.再比较优先级
                if (requireApp.priority > loginApp.priority) {
                    //①”请求App“的优先级高，”已注册App“加入deleteList，等待被删
                    deleteList.add(j);
                } else {
                    //但凡低于registeredList中任一元素，则跳出循环，来到下一个”请求App“
                    //②”请求App“的优先级低
                    flag = true;
                    break;
                }
            }
            if (flag) {
                continue;
            }
            //3.”请求App“的优先级高于registeredList中所有有交叉的元素，则倒着删除它们
            Collections.reverse(deleteList);
            for (int idx : deleteList) {
                loginList.remove(idx);
            }
            //4.”请求App“被注册上
            loginList.add(requireApp);
        }

        String result = "NA";
        for (App registered : loginList) {
            if (registered.start <= queryTime && queryTime < registered.end) {
                result = registered.name;
                break;
            }
        }
        return result;
    }
}