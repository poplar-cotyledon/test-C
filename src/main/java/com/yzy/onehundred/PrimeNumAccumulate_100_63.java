package com.yzy.onehundred;

import java.util.Scanner;

public class PrimeNumAccumulate_100_63 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int result = sc.nextInt();

        for (int i = 2; i <= Math.sqrt(result); i++) {
            if (result % i == 0 && isPrimeNum(i)) {
                //找到第一个素数i
                int j = result / i;
                if (isPrimeNum(j)) {
                    //找到第二个素数j
                    System.out.println(i + " " + j);
                    return;
                }
            }
        }
        System.out.println("-1 -1");
    }

    private static boolean isPrimeNum(int num) {
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                //不是素数
                return false;
            }
        }
        //是素数
        return true;
    }
}
