package com.yzy.onehundred;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringJoiner;

public class DecomposeNum_100_20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();

        int start = 1;
        int end = start + 1;
        int sum = start + end;
        Deque<String> deque = new LinkedList<>();//队列
        while (start < end) {
            if(sum < num){
                end++;
                sum += end;
            }else if(sum > num){
                sum -= start;
                start++;
            }else{
                StringBuilder sb = new StringBuilder(num + "=");
                for (int i = start; i < end;i++) {
                    sb.append(i).append("+");
                }
                sb.append(end);
                deque.push(sb.toString());
                sum -= start;
                start++;
            }
        }
        if(deque.isEmpty()){
            System.out.println("N");
        }else {
            //出栈——最短的分解序列在栈顶
            System.out.println(deque.pollLast());
        }
    }
}