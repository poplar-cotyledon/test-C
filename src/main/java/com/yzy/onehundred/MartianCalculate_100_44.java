package com.yzy.onehundred;

import java.util.Scanner;

public class MartianCalculate_100_44 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] splits = sc.nextLine().split("\\$");//7#6#2 5 9#12
        int result = 0;
        for (int i = 0; i < splits.length; i++) {
            String[] sub = splits[i].split("\\#");//7 6 2
            int number = Integer.parseInt(sub[0]);
            for (int j = 1; j < sub.length; j++) {
                number = Hashtag(number, Integer.parseInt(sub[j]));
            }
            if (i == 0) {
                result = number;
            } else {
                result = dollar(result, number);
            }
        }
        System.out.println(result);
    }

    private static int Hashtag(int x, int y) {
        return 4 * x + 3 * y + 2;
    }

    private static int dollar(int x, int y) {
        return 2 * x + y + 3;
    }
}
