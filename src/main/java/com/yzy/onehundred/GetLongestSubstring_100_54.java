package com.yzy.onehundred;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GetLongestSubstring_100_54 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int flaw = Integer.parseInt(sc.nextLine());
        char[] chars = sc.nextLine().toCharArray();

        List<Character> vowel = new ArrayList<>();
        vowel.add('a');
        vowel.add('e');
        vowel.add('i');
        vowel.add('o');
        vowel.add('u');
        vowel.add('A');
        vowel.add('E');
        vowel.add('I');
        vowel.add('O');
        vowel.add('U');

        List<Integer> vowel_index = new ArrayList<>();
        for (int i = 0; i < chars.length; i++) {
            if (vowel.contains(chars[i])){
                vowel_index.add(i);
            }
        }

        int result = 0;
        int n = vowel_index.size();
        int l = 0;
        int r = 0;
        while (r < n) {
            int diff = vowel_index.get(r) - vowel_index.get(l) - (r - l);//瑕疵度计算
            if (diff > flaw) {
                l++;
            } else if (diff < flaw) {
                r++;
            } else {
                result = Math.max(result, vowel_index.get(r) - vowel_index.get(l) + 1);
                r++;
            }
        }
        System.out.println(result);
    }
}