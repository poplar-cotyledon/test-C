package com.yzy.onehundred;

import java.util.Scanner;

public class PasswordDecrypt_100_43 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] split = sc.nextLine().split("\\*");
        StringBuilder sb = new StringBuilder();
        for (String s : split) {
            if (s.length() > 2) {
                char[] chars = s.toCharArray();
                for (int i = 0; i < chars.length - 2; i++) {
                    sb.append((char) (Integer.parseInt(String.valueOf(chars[i])) + 96));
                }
                sb.append((char) (Integer.parseInt(s.substring(s.length() - 2)) + 96));
            } else {
                sb.append((char) (Integer.parseInt(s) + 96));
            }
        }
        System.out.println(sb);
    }
}
