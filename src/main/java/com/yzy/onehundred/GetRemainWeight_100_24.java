package com.yzy.onehundred;

import java.util.*;

public class GetRemainWeight_100_24 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        LinkedList<Integer> remains = new LinkedList<>();
        for (int i = 0; i < num; i++) {
            remains.addLast(sc.nextInt());
        }

        remains.sort(Comparator.comparingInt(o -> o));//升序排
        while (remains.size() > 2) {
            int z = remains.removeLast();
            int y = remains.removeLast();
            int x = remains.removeLast();
            int remain = Math.abs((z - y) - (y - x));
            if (remain != 0) {
                if (!remains.isEmpty()) {
                    int loc = Math.abs(Collections.binarySearch(remains, remain) + 1);
                    remains.add(loc, remain);
                } else {
                    System.out.println(remain);
                    return;
                }
            }
        }
        if (remains.size() == 1) {
            System.out.println(remains.get(0));
        } else if (remains.size() == 2) {
            System.out.println(Math.max(remains.get(0), remains.get(1)));
        } else {
            System.out.println(0);
        }
    }
}
