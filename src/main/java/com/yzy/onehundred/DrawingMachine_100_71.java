package com.yzy.onehundred;

import java.util.Scanner;

public class DrawingMachine_100_71 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int end = sc.nextInt();
        int[][] loc = new int[num + 2][2];
        for (int i = 1; i < num + 1; i++) {
            for (int j = 0; j < 2; j++) {
                loc[i][j] = sc.nextInt();
            }
        }
        loc[num + 1][0] = end;

        int[] height = new int[num + 1];
        int area = 0;
        for (int i = 1; i < num + 1; i++) {
            height[i] = Math.abs(loc[i][1] + height[i - 1]);
            area += (loc[i + 1][0] - loc[i][0]) * height[i];
        }
        System.out.println(area);
    }
}