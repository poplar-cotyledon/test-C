package com.yzy.onehundred;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class MaxCoordinateValue_100_26 {
    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            int num = sc.nextInt();
            int lucky = sc.nextInt();
            //非法输入拦截
            if (num < 1 || num > 100 || lucky < -100 || lucky > 100) {
                throw new Exception();
            }
            List<Integer> commands = new ArrayList<>();
            for (int i = 0; i < num; i++) {
                int c = sc.nextInt();
                //非法输入拦截
                if (c < -100 || c > 100) {
                    throw new Exception();
                }
                commands.add(c);
            }

            List<Integer> coordinateValue = new ArrayList<>();
            coordinateValue.add(0);
            int sum = 0;
            for (int i = 0; i < commands.size(); i++) {
                int c = commands.get(i);
                sum += c;
                if (c == lucky && lucky < 0) {
                    sum--;
                } else if (c == lucky && lucky > 0) {
                    sum++;
                }
                coordinateValue.add(sum);
            }
            Collections.sort(coordinateValue);
            System.out.println(coordinateValue.get(num));

        } catch (Exception e) {
            System.out.println(12345);
        }
    }
}
