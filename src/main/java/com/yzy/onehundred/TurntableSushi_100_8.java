package com.yzy.onehundred;

import java.util.Arrays;
import java.util.Scanner;
import java.util.StringJoiner;

public class TurntableSushi_100_8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] prices = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        StringJoiner sj = new StringJoiner(" ");
        for (int i = 0; i < prices.length; i++) {
            int price = prices[i];
            for (int j = (i + 1) % prices.length; j != i; j = (j + 1) % prices.length) {
                if (prices[i] > prices[j]) {
                    price += prices[j];
                    break;
                }
            }
            sj.add(String.valueOf(price));
        }
        System.out.println(sj);
    }
}
