package com.yzy.onehundred;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GreedyAlgorithm1_100_24 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            String s = sc.nextLine();
            int[] arr = Arrays.stream(s.split(" ")).mapToInt(Integer::parseInt).toArray();
            System.out.println(getResult(arr));
        } catch (Exception e) {
            System.out.println(0);//什么也不输入，就是0
        }
    }

    private static int getResult(int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i : arr) {
            if (!map.containsKey(i)) {
                map.put(i, 1);
            } else {
                map.put(i, map.get(i) + 1);
            }
        }
        int result = 0;//总人数
        for (Integer key : map.keySet()) {
            int children = key + 1;//小区总人数
            double value = 1.0 * map.get(key);//有几个孩子说
            result += Math.ceil(value / children) * children;
        }
        return result;
    }
}
