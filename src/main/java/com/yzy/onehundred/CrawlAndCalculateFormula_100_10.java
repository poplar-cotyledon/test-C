package com.yzy.onehundred;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CrawlAndCalculateFormula_100_10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();

        //得到最长合法简单数学表达式
        String maxLenExp = getMaxLenExp(input);
        if (maxLenExp.length() == 0) {
            //未找到，直接输出0
            System.out.println("0");
        } else {
            //找到了，计算中缀表达式
            System.out.println(calculateExp1(maxLenExp));
        }
    }

    //得到最长合法简单数学表达式
    public static String getMaxLenExp(String s) {
        //1.定义爬取规则
        String regex = "([+-]?(\\d+[+*-])*\\d+)";
        //2.将正则表达式封装成一个pattern对象
        Pattern pattern = Pattern.compile(regex);
        //3.通过pattern对象获取查找内容的匹配器对象
        Matcher matcher = pattern.matcher(s);

        String maxLenExp = "";
        //4.定义循环开始爬取信息
        while (matcher.find()) {
            String exp = matcher.group(0);
            if (exp.length() > maxLenExp.length()) {
                maxLenExp = exp;
            }
        }
        return maxLenExp;
    }

    //计算中缀表达式
    //方法一：🐱
    public static int calculateExp1(String exp) {
        Stack<Integer> stack = new Stack<>();
        StringBuilder sb = new StringBuilder();
        int sigh = 1;//标记正负数

        //处理第一个字符为运算符的情况
        char start = exp.charAt(0);
        if (start == '+' || start == '-') {
            exp = exp.substring(1);//从第二个字符开始截取算式
        }
        if (start == '-') {
            sigh = -1;//第一个数为负数
        }

        //扫描表达式
        exp += "+0";//便于最后一轮计算
        for (char c : exp.toCharArray()) {
            if (c >= '0' && c <= '9') {
                sb.append(c);
                continue;
            }
            stack.push(sigh * Integer.parseInt(sb.toString()));
            sb = new StringBuilder();
            if (c == '+') {
                sigh = 1;
            } else if (c == '-') {
                sigh = -1;
            } else if (c == '*') {
                sigh = stack.pop();
            }
        }

        //算式变成纯加减，遍历累加求和即得结果
        int result = 0;
        while (!stack.isEmpty()) {
            result += stack.pop();
        }
        return result;
    }

    //方法二：
    public static int calculateExp2(String exp) {
        //设置优先级
        Map<Character, Integer> map = new HashMap<>();
        map.put('+', 0);
        map.put('-', 0);
        map.put('*', 1);
        //初始化栈
        Stack<Integer> numStack = new Stack<>();//操作数栈
        Stack<Character> signStack = new Stack<>();//运算符栈+-*
        //扫描表达式
        exp += "+0";//便于最后一轮计算
        StringBuilder num = new StringBuilder();
        for (char c : exp.toCharArray()) {
            //一、扫描到操作数
            if (c >= '0' && c <= '9') {
                num.append(c);
                continue;
            }
            //操作数入栈
            if (num.length() > 0) { //避免第一个字符为运算符
                numStack.push(Integer.valueOf(num.toString()));
                num = new StringBuilder();
            }
            //二、扫描到运算符
            if (signStack.empty() || map.get(c) > map.get(signStack.peek())) {
                //1.运算符栈为空 || 当前运算符的优先级>栈顶运算符的优先级，直接入栈
                signStack.push(c);
            } else {
                //2.运算符栈非空 && 当前运算符的优先级<=栈顶运算符的优先级
                while (!signStack.isEmpty()) {
                    int right = numStack.pop();//右操作数
                    int left = 0;
                    if (!numStack.isEmpty()) { //避免第一个字符为运算符
                        left = numStack.pop();//左操作数
                    }
                    char sigh = signStack.pop();//运算符
                    int result;//运算结果
                    if (sigh == '+') {
                        result = left + right;
                    } else if (sigh == '-') {
                        result = left - right;
                    } else {
                        result = left * right;
                    }
                    numStack.push(result);
                }
                signStack.push(c);
            }
        }
        return numStack.pop();
    }
}
