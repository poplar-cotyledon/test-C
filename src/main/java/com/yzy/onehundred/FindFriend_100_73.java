package com.yzy.onehundred;

import java.util.*;

public class FindFriend_100_73 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        System.out.println(getResult(arr));
    }

    public static String getResult(int[] arr) {
        int len = arr.length;
        int[] result = new int[len];
        Stack<int[]> stack = new Stack<>();
        for (int i = 0; i < len; i++) {
            int num = arr[i];

            while (true) {
                if (stack.size() == 0) {
                    stack.push(new int[]{num, i});
                    break;
                }

                int[] peek = stack.peek();
                int peekNum = peek[0];
                int peekIdx = peek[1];

                if (num > peekNum) {
                    result[peekIdx] = i;
                    stack.pop();
                } else {
                    stack.add(new int[]{num, i});
                    break;
                }
            }
        }

        StringJoiner sj = new StringJoiner(" ");
        for (int v : result) {
            sj.add(v + "");
        }
        return sj.toString();
    }
}
