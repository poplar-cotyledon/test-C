package com.yzy.onehundred;

import java.util.Arrays;
import java.util.Scanner;
import java.util.StringJoiner;

public class DataCompression_100_40 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        int[] loc = Arrays.stream(s.split(" ")).mapToInt(Integer::parseInt).toArray();

        //1.输入数据只有2个坐标点
        if (loc.length == 4) {
            System.out.println(s);
            return;
        }
        //2.输入数据有3及以上个坐标点——3个坐标1组
        StringJoiner sj = new StringJoiner(" ");
        //起点
        sj.add(String.valueOf(loc[0]));
        sj.add(String.valueOf(loc[1]));
        //拐点
        for (int i = 2; i < loc.length - 2; i += 2) {
            if (!isValid(loc[i - 2], loc[i - 1], loc[i], loc[i + 1], loc[i + 2], loc[i + 3])) {
                sj.add(String.valueOf(loc[i]));
                sj.add(String.valueOf(loc[i + 1]));
            }
        }
        //终点
        sj.add(String.valueOf(loc[loc.length - 2]));
        sj.add(String.valueOf(loc[loc.length - 1]));
        System.out.println(sj);
    }

    private static Boolean isValid(int a, int b, int x, int y, int c, int d) {
        //与前一个坐标重复 || 上下-列同且同向 || 左右-行相同且同向 || 左下右上-坐标和相同且同向 || 右下左上-坐标差相同且同向
        return (a == x && b == y)
                || (b == y && d == y && (x - a) == (c - x))
                || (a == x && c == x && (y - b) == (d - y))
                || ((a + b) == (x + y) && (x + y) == (c + d) && (x - a) == (c - x))
                || ((x - a) == (y - b) && (c - x) == (d - y) && (x - a) == (c - x));
    }
}
