package com.yzy.onehundred;

import java.util.Scanner;

public class ExoticGuests_100_17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int value = sc.nextInt();//物价
        int luckyNum = sc.nextInt();//幸运数字
        int base = sc.nextInt();//进制

        int a = value;
        int b = 0;
        int count = 0;
        while (a / base > 0) {
            b = a % base;
            a = a / base;
            if(b==luckyNum){
                count++;
            }
        }
        if(a==luckyNum){
            count++;
        }
        System.out.println(count);
    }
}
