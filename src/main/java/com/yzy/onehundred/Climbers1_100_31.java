package com.yzy.onehundred;

import java.util.Arrays;
import java.util.Scanner;

public class Climbers1_100_31 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        StringBuilder sb = new StringBuilder();
        sb.append(0).append(",").append(sc.nextLine()).append(",").append(0);
        sb.deleteCharAt(sb.length() - 3);
        sb.deleteCharAt(2);
        int[] mountains = Arrays.stream(sb.toString().split(",")).mapToInt(Integer::parseInt).toArray();

        int peakCount = 0;
        for (int i = 0; i < mountains.length - 2; i++) {
            //山峰：左右高度均低于自己
            if (mountains[i + 1] > mountains[i] && mountains[i + 1] > mountains[i + 2]) {
                peakCount++;
            }
        }
        System.out.println(peakCount);
    }
}
