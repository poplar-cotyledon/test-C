package com.yzy.onehundred;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class SplitBalancedString_100_18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();

        char[] chars = s.toCharArray();
        List<Character> xList = new LinkedList<>();
        List<Character> yList = new LinkedList<>();
        int count = 0;//计数器
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == 'X') {
                xList.add(chars[i]);
            } else {
                yList.add(chars[i]);
            }
            if (xList.size() > 0 && yList.size() > 0 && xList.size() == yList.size()) {
                count++;
            }
        }
        System.out.println(count);
    }
}
