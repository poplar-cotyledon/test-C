package com.yzy.onehundred;

import java.util.Scanner;

public class ReliableCar_100_14 {
    static int count = 0;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();//100
        init(N);
        System.out.println(count);
    }

    static void init(int max) {
        int idx = max;
        while (idx > 0) {
            idx = check(idx);
            count++;
            idx--;
        }
    }

    static int check(int num) {
        int res = num;
        String str = String.valueOf(num).replace("4", "3");
        return Integer.valueOf(str);
    }
}