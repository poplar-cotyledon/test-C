package com.yzy.onehundred;

import java.util.Arrays;
import java.util.Scanner;

public class GameGrouping_100_40 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] scores = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        int sum = 0;
        for (int score : scores) {
            sum += score;
        }
        int sumA = sum / 2;
        int sumB = sum % 2 == 0 ? sumA : sumA + 1;
        int countA = 5;
        int countB = 5;
        Arrays.sort(scores);
        for (int i = scores.length - 1; i >= 0; i--) {
            if (sumB >= sumA) {
                if (countB > 0) {
                    sumB -= scores[i];
                    countB--;
                } else {
                    sumA -= scores[i];
                    countA--;
                }
            } else {
                if (countA > 0) {
                    sumA -= scores[i];
                    countA--;
                } else {
                    sumB -= scores[i];
                    countB--;
                }
            }
        }
        System.out.println(Math.abs(sumA) + Math.abs(sumB) + sum % 2);
    }
}
