package com.yzy.onehundred;

import java.util.*;

public class LineByHeightAndWeight_100_31 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int studentNum = Integer.parseInt(sc.nextLine());
        int[] height = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int[] weight = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        Map<Integer, List<Integer>> students = new HashMap<>();
        for (int i = 0; i < studentNum; i++) {
            List<Integer> list = new ArrayList<>();
            list.add(height[i]);
            list.add(weight[i]);
            students.put(i + 1, list);
        }

        List<Map.Entry<Integer, List<Integer>>> studentList = new ArrayList<>(students.entrySet());
        studentList.sort((o1, o2) -> {
            if (!Objects.equals(o1.getValue().get(0), o2.getValue().get(0))
                    && !Objects.equals(o1.getValue().get(1), o2.getValue().get(1))) {
                //按照身高由低到高排列
                return o1.getValue().get(0) - o2.getValue().get(0);
            } else if (Objects.equals(o1.getValue().get(0), o2.getValue().get(0))
                    && !Objects.equals(o1.getValue().get(1), o2.getValue().get(1))) {
                //对身高相同的人按体重由轻到重排列
                return o1.getValue().get(1) - o2.getValue().get(1);
            } else {
                //对于身高体重都相同的人，维持原有的编号顺序关系
                return 0;
            }
        });
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Integer, List<Integer>> entry : studentList) {
            sb.append(entry.getKey()).append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);
        System.out.println(sb);
    }
}