package com.yzy.onehundred;

import java.util.*;

public class WeightedSumSort_100_9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int w1 = sc.nextInt();
        int w2 = sc.nextInt();
        int w3 = sc.nextInt();
        int w4 = sc.nextInt();
        int w5 = sc.nextInt();
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < num; i++) {
            String name = sc.next();
            int nr_watch = sc.nextInt();
            int nr_star = sc.nextInt();
            int nr_fork = sc.nextInt();
            int nr_issue = sc.nextInt();
            int nr_mr = sc.nextInt();
            int weightedSum = w1 * nr_watch + w2 * nr_star + w3 * nr_fork + w4 * nr_issue + w5 * nr_mr;
            map.put(name, weightedSum);
        }

        //排序
        List<String> list = new ArrayList<>(map.keySet());
        list.sort((a, b) -> {
            //值不相等，根据value排降序
            if (!Objects.equals(map.get(a), map.get(b))) {
                return map.get(b) - map.get(a);
            }
            //值相等，根据key排升序
            return a.compareTo(b);
        });
        list.forEach(System.out::println);
    }
}