package com.yzy.onehundred;

import java.util.*;

public class TestCaseExecutePlan_100_41 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] arr = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int featureNum = arr[0];
        int testNum = arr[1];
        Map<Integer, Integer> priorMap = new HashMap<>();
        for (int i = 1; i <= featureNum; i++) {
            priorMap.put(i, Integer.parseInt(sc.nextLine()));
        }
        Map<Integer, int[]> relateMap = new HashMap<>();
        for (int i = 1; i <= testNum; i++) {
            relateMap.put(i, Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray());
        }

        Map<Integer, Integer> executeMap = new HashMap<>();
        relateMap.forEach((testId, featureIds) -> {
            int priorSum = 0;
            for (int featureId : featureIds) {
                priorSum += priorMap.get(featureId);
            }
            executeMap.put(testId, priorSum);
        });
        List<Integer> executeList = new ArrayList<>(executeMap.keySet());
        executeList.sort((a, b) -> {
            if (!Objects.equals(executeMap.get(a), executeMap.get(b))) {
                return executeMap.get(b) - executeMap.get(a);
            } else {
                return a - b;
            }
        });
        executeList.forEach(System.out::println);
    }
}
