package com.yzy.onehundred;

import java.util.*;

public class GetSumOfSurvivalNum_100_49 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        String sub = s.substring(1);
        String[] nums = sub.split("[\\[\\],]");

        List<Integer> numsList = new ArrayList<>();
        for (int i = 0; i < nums.length - 3; i++) {
            numsList.add(Integer.valueOf(nums[i]));
        }
        int jumpNum = Integer.parseInt(nums[nums.length - 2]);
        int survivalNum = Integer.parseInt(nums[nums.length - 1]);
        if (survivalNum == 0) {
            System.out.println(0);
            return;
        }

        for (int i = (jumpNum + 1) % nums.length; numsList.size() > survivalNum; i = (i + jumpNum) % numsList.size()) {
            numsList.remove(i);
        }
        int sum = 0;
        for (Integer num : numsList) {
            sum += num;
        }
        System.out.println(sum);
    }
}
