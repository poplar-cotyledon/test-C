package com.yzy.onehundred;

import java.util.Arrays;
import java.util.Scanner;

public class GetMaxWeightSumOfBinaryTree_100_59 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        if (s.isEmpty()) {
            System.out.println(0);
            return;
        }
        int[] nums = Arrays.stream(s.split(" ")).mapToInt(Integer::parseInt).toArray();

        int i = nums.length - 1;
        while (i >= 2) {
            if (i % 2 == 0) {
                //i为偶数，则i的父节点的值+=两个子节点中的大者
                int max = Math.max(nums[i], nums[i - 1]);
                if (max != -1) {
                    nums[(i / 2) - 1] += max;
                }
                i -= 2;
            } else {
                //i为奇数，则i的父节点的值+=两个子节点中的大者
                int max = nums[i];
                if (max != -1) {
                    nums[i / 2] += max;
                }
                i--;
            }
        }
        System.out.println(nums[0]);
    }
}
