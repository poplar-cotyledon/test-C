package com.yzy.onehundred;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CPUAllocation_100_28 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int cpuNumA = sc.nextInt();
        int cpuNumB = sc.nextInt();
        List<Integer> cpuA = new ArrayList<>();
        for (int i = 0; i < cpuNumA; i++) {
            cpuA.add(sc.nextInt());
        }
        List<Integer> cpuB = new ArrayList<>();
        for (int i = 0; i < cpuNumB; i++) {
            cpuB.add(sc.nextInt());
        }

        for (int i = 0; i < cpuNumA; i++) {
            for (int j = 0; j < cpuNumB; j++) {
                int a = cpuA.get(i);
                int b = cpuB.get(j);
                if (sum(cpuA) != sum(cpuB)) {
                    if ((sum(cpuA) - a + b) == (sum(cpuB) - b + a)) {
                        //交换后算力相等
                        System.out.println(a + " " + b);
                        return;
                    }
                }
            }
        }
    }

    //cpu算力求和
    private static int sum(List<Integer> cpu) {
        int result = 0;
        for (int i = 0; i < cpu.size(); i++) {
            result += cpu.get(i);
        }
        return result;
    }
}
