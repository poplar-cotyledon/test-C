package com.yzy.onehundred;

import java.util.Scanner;
import java.util.StringJoiner;

public class SpiralDigitalMatrix_100_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();// 需要在螺旋矩阵中填入 1 ~ n 数字
        int m = sc.nextInt();// 螺旋矩阵行数
        int k = (int) Math.ceil(1.0 * n / m);// 螺旋矩阵列数

        int[][] matrix = new int[m][k];// 螺旋矩阵
        int step = 1;// 当前要填入的值
        int x = 0;
        int y = 0;
        while (step <= n) {
            //右行
            while (y < k && matrix[x][y] == 0 && step <= n) {
                matrix[x][y++] = step++;
            }
            y--;// 正序填完第x行后，y处于末尾越界位置，因此y需要退一步
            x++;// 正序填完第x行来到第x行的末尾，即第y列，按照螺旋矩阵顺序，应该从第x+1行开始正序填值第y列

            //下行
            while (x < m && matrix[x][y] == 0 && step <= n) {
                matrix[x++][y] = step++;
            }
            x--;
            y--;

            //左行
            while (y >= 0 && matrix[x][y] == 0 && step <= n) {
                matrix[x][y--] = step++;
            }
            y++;
            x--;

            //上行
            while (x >= 0 && matrix[x][y] == 0 && step <= n) {
                matrix[x--][y] = step++;
            }
            x++;
            y++;
        }

        // 打印螺旋矩阵字符串
        for (int i = 0; i < m; i++) {
            StringJoiner row = new StringJoiner(" ");
            for (int j = 0; j < k; j++) {
                if (matrix[i][j] == 0) {
                    row.add("*");
                } else {
                    row.add(matrix[i][j] + "");
                }
            }
            System.out.println(row);
        }
    }
}