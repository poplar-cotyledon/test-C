package com.yzy.onehundred;

import java.util.*;

public class AirportFlightsSort_100_30 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        String[] airplanes = s.split(",");

        //升序
        Arrays.sort(airplanes, (o1, o2) -> {
            int i = 0;
            while(i < 6) {
                if(o1.charAt(i) == o2.charAt(i)){
                    i++;
                }else {
                    return o1.charAt(i) - o2.charAt(i);
                }
            }
            return 0;
        });

        StringJoiner sj =new StringJoiner(",");
        for (String plane : airplanes) {
            sj.add(plane);
        }
        System.out.println(sj);
    }
}