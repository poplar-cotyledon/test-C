package com.yzy.onehundred;

import java.util.Scanner;

public class GetTreeHeight_100_55 {
    //定义三叉树的结点
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode mid;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] nums = new int[n];
        for (int i = 0; i < n; i++) {
            nums[i] = sc.nextInt();
        }

        //第一个结点为根结点
        TreeNode root = new TreeNode(nums[0]);
        //根结点的自身层数为1
        int maxHeight = 1;
        //从第二个结点开始，往根结点下插入
        for (int i = 1; i < n; i++) {
            int height = insertIntoTree(root, nums[i]);
            maxHeight = Math.max(maxHeight, height);
        }
        System.out.println(maxHeight);
    }

    private static int insertIntoTree(TreeNode root, int num) {
        //进入循环，就说明树的初始层高为2
        int height = 2;
        while (true) {
            int diff = num - root.val;//差值
            if (Math.abs(diff) <= 500) {
                //中子树
                if (root.mid == null) {
                    //若中子树下没有结点，插入返回
                    root.mid = new TreeNode(num);
                    break;
                } else {
                    //若中子树下有结点，该结点变为根结点
                    root = root.mid;
                    height++;
                }
            } else if (diff < -500) {
                //左子树
                if (root.left == null) {
                    root.left = new TreeNode(num);
                    break;
                } else {
                    root = root.left;
                    height++;
                }
            } else {
                //右子树
                if (root.right == null) {
                    root.right = new TreeNode(num);
                    break;
                } else {
                    root = root.right;
                    height++;
                }
            }
        }
        return height;
    }
}