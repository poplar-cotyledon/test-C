package com.yzy.onehundred;

import java.util.*;

public class FindBestTimePeriod_100_11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int minAverageLost = Integer.parseInt(sc.nextLine());
        String times = sc.nextLine();
        String[] timeStr = times.split(" ");
        List<Integer> timeArr = new ArrayList<>();
        for (String str : timeStr) {
            timeArr.add(Integer.parseInt(str));
        }

        Map<Integer, List<Integer>> loc = new HashMap<>();
        for (int i = 0; i < timeArr.size(); i += loc.get(i).size() + 1) {
            int sum = 0;
            List<Integer> vs = new ArrayList<>();
            loc.put(i, vs);
            for (int j = i; j < timeArr.size(); j++) {
                sum += timeArr.get(j);
                if (sum > minAverageLost * (j - i + 1)) {
                    break;
                } else {
                    vs.add(j);
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        loc.forEach((k, vs) -> {
            if (vs != null && vs.size() > 0) {
                sb.append(k).append("-").append(vs.get(vs.size() - 1)).append(" ");
            }
        });
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
            System.out.println(sb);
        } else {
            System.out.println("NULL");
        }
    }
}