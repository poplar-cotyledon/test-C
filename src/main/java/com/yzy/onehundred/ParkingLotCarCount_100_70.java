package com.yzy.onehundred;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ParkingLotCarCount_100_70 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            int[] arr = Arrays.stream(sc.nextLine().split(",")).mapToInt(Integer::parseInt).toArray();
            System.out.println(getResult(arr));
        } catch (Exception e) {
            System.out.println(0);//什么也不输入，就是0
        }
    }

    private static int getResult(int[] arr) {
        List<Integer> list = new ArrayList<>();
        int count1 = 0;
        int count0 = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 1) {
                if (count1 == 0) {
                    count0 = 0;
                }
                count1++;
            } else {
                if (count0 == 0) {
                    list.add(count1);
                    count1 = 0;
                }
                count0++;
            }
        }
        if (count1 != 0) {
            list.add(count1);
        }

        int result = 0;
        for (Integer i : list) {
            result += i / 3 + (i % 3) / 2 + (i % 3) % 2;
        }
        return result;
    }
}
