package com.yzy.onehundred;

import java.util.Scanner;

public class GetMinSum_100_46 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        char[] chars = s.toCharArray();

        int sum = 0;
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (c == '-') {
                i++;
                int start = i;
                while (i < chars.length && Character.isDigit(chars[i])) {
                    i++;
                }
                //字符串s从下标为start的位置开始截取到下标为i的位置的值（不包括下标为i的值）
                String substring = s.substring(start, i);
                if (substring.length() > 0) {
                    sum -= Integer.parseInt(substring);
                }
                i--;
                continue;
            }
            if (Character.isDigit(c)) {
                sum += Character.digit(c, 10);
            }
        }
        System.out.println(sum);
    }
}
