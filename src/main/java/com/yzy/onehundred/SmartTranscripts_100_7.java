package com.yzy.onehundred;

import java.util.*;

public class SmartTranscripts_100_7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int students = sc.nextInt();
        int subjects = sc.nextInt();
        List<String> subjectList = new ArrayList<>();
        for (int i = 0; i < subjects; i++) {
            subjectList.add(sc.next());
        }
        subjectList.add("zongfen");
        Map<String, List<Integer>> studentTranscript = new HashMap<>();
        for (int i = 0; i < students; i++) {
            String name = sc.next();
            List<Integer> scoreList = new ArrayList<>();
            int sum = 0;
            for (int j = 0; j < subjects; j++) {
                int score = sc.nextInt();
                sum += score;
                scoreList.add(score);
            }
            scoreList.add(sum);
            studentTranscript.put(name, scoreList);
        }
        String sortMark = sc.next();

        int index = subjectList.indexOf(sortMark);
        //排序
        List<Map.Entry<String, List<Integer>>> entries = new ArrayList<>(studentTranscript.entrySet());
        entries.sort((o1, o2) -> {
            if (!o1.getValue().get(index).equals(o2.getValue().get(index))) {
                //分数不同，根据分数排降序
                return o2.getValue().get(index) - o1.getValue().get(index);
            } else {
                //分数相同，根据姓名排升序
                return o1.getKey().compareTo(o2.getKey());
            }
        });

        StringBuilder sb = new StringBuilder();
        //遍历输出姓名
        for (Map.Entry<String, List<Integer>> entry : entries) {
            sb.append(entry.getKey()).append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);
        System.out.println(sb);
    }
}
