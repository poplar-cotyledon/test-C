package com.yzy.onehundred;

import java.util.*;

public class FrequencyTags_100_12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] arr = new int[num];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }
        int tag = sc.nextInt();

        Map<Integer, Integer> map = new HashMap<>();
        for (Integer a : arr) {
            if (!map.containsKey(a)) {
                map.put(a, 1);
            } else {
                map.put(a, map.get(a) + 1);
            }
        }

        List<Integer> list = new ArrayList<>(map.keySet())
                .stream()
                .filter(a -> map.get(a) >= tag)
                .sorted().sorted((a, b) -> {
                    if (!Objects.equals(map.get(a), map.get(b))) {
                        return map.get(a) - map.get(b);
                    }
                    return a - b;
                })
                .toList();

        if (list.isEmpty()) {
            System.out.println(0);
        } else {
            System.out.println(list.size());
            list.forEach(System.out::println);
        }
    }
}
