package com.yzy.onehundred;

import java.util.Scanner;

public class StringDetermine_100_60 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char[] chars1 = sc.nextLine().toCharArray();
        char[] chars2 = sc.nextLine().toCharArray();

        int index = -1;
        int i = 0;
        int j = 0;
        while (i < chars1.length && j< chars2.length) {
            if(chars1[i]==chars2[j]){
                index = j;
                i++;
            }
            j++;
        }
        System.out.println(index);
    }
}
