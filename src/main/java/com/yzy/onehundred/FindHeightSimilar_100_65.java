package com.yzy.onehundred;

import java.util.*;

public class FindHeightSimilar_100_65 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int xmHeight = sc.nextInt();
        int otherChildNum = sc.nextInt();
        List<Integer> otherChildHeight = new ArrayList<>();
        for (int i = 0; i < otherChildNum; i++) {
            otherChildHeight.add(sc.nextInt());
        }

        //1.集合根据"身高差值的绝对值"排升序，相等则根据"身高"排升序
        otherChildHeight.sort((a, b) -> {
            int diffA = Math.abs(a - xmHeight);
            int diffB = Math.abs(b - xmHeight);
            if (diffA != diffB) {
                return diffA - diffB;
            }
            return a - b;
        });

        //2.遍历打印
        StringJoiner sj = new StringJoiner(" ");
        for (Integer height : otherChildHeight) {
            sj.add(height+"");
        }
        System.out.println(sj);
    }
}
