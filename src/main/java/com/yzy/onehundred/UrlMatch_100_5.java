package com.yzy.onehundred;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UrlMatch_100_5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//访问历史日志的条数，0<N<=100。
        List<String[]> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            String s = sc.next();
            String[] split = s.split("/");
            list.add(split);
        }
        int level = sc.nextInt();
        String keyWord = sc.next();
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            String[] strings = list.get(i);
            if (strings.length <= level) {
                break;
            }
            if (strings[level].equals(keyWord)) {
                count++;
            }
        }
        System.out.println(count);
    }
}
