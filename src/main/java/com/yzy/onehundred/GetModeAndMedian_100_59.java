package com.yzy.onehundred;

import java.util.*;

public class GetModeAndMedian_100_59 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] nums = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        //1.统计各数字出现次数
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            if (!map.containsKey(num)) {
                map.put(num, 1);
            } else {
                map.put(num, map.get(num) + 1);
            }
        }

        //2.将众数遍历挑选到modeList
        List<Integer> numList = new ArrayList<>(map.keySet());
        List<Integer> modeList = new ArrayList<>();
        int max = 0;
        for (Integer key : numList) {
            if (map.get(key) > max) {
                modeList.clear();
                max = map.get(key);
                modeList.add(key);
            } else if (map.get(key) == max) {
                modeList.add(key);
            }
        }

        //3.将众数排升序，求中位数
        modeList.sort(Comparator.comparingInt(o -> o));
        if (modeList.size() % 2 == 0) {
            System.out.println((modeList.get((modeList.size() / 2) - 1) + modeList.get(modeList.size() / 2)) / 2);
        } else {
            System.out.println(modeList.get(modeList.size() / 2));
        }
    }
}
