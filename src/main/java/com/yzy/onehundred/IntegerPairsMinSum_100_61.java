package com.yzy.onehundred;

import java.util.*;

public class IntegerPairsMinSum_100_61 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //接收输入数据
        int array1Size = sc.nextInt();
        List<Integer> array1 = new ArrayList<>();
        for (int i = 0; i < array1Size; i++) {
            array1.add(sc.nextInt());
        }

        //将2个数组数据依次相加，结果有array1Size * array2Size种，存入集合并排升序，取出前k个数据相加输出最终结果最小和
        int array2Size = sc.nextInt();
        List<Integer> arraySum = new ArrayList<>();
        for (int i = 0; i < array2Size; i++) {
            int elem2 = sc.nextInt();
            for (int j = 0; j < array1.size(); j++) {
                arraySum.add(elem2 + array1.get(j));
            }
        }
        int k = sc.nextInt();
        Collections.sort(arraySum);

        int minSum = 0;
        for (int i = 0; i < k; i++) {
            minSum += arraySum.get(i);
        }
        System.out.println(minSum);
    }
}
