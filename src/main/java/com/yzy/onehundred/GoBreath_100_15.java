package com.yzy.onehundred;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class GoBreath_100_15 {
    private static final int maxSize = 19;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] blackValue = sc.nextLine().split(" ");//0 5 8 9 9 10
        String[] whiteValue = sc.nextLine().split(" ");//5 0 9 9 9 8
        String[] black = transform(blackValue);
        String[] white = transform(whiteValue);
        System.out.println(count(black, white) + " " + count(white, black));
    }

    private static int count(String[] our, String[] rival) {
        //用set统计——能够覆盖重复值
        Set<String> value = new HashSet<>();
        //往set统计自己的上下左右(若有)
        for (int i = 0; i < our.length; i++) { //3
            String[] s = our[i].split(",");
            int row = Integer.parseInt(s[0]); //行
            int column = Integer.parseInt(s[1]); //列
            if (row > 0) {
                value.add((row - 1) + "," + column);//上
            }
            if (row < maxSize) {
                value.add((row + 1) + "," + column);//下
            }
            if (column > 0) {
                value.add(row + "," + (column - 1));//左
            }
            if (column < maxSize) {
                value.add(row + "," + (column + 1));//右
            }
        }
        //移除set中与自己棋、对手棋相同的值
        for (int i = 0; i < our.length; i++) {
            if(value.contains(our[i])){
                value.remove(our[i]);
            }
        }
        for (int i = 0; i < rival.length; i++) {
            if(value.contains(rival[i])){
                value.remove(rival[i]);
            }
        }
        return value.size();
    }

    private static String[] transform(String[] value) { //6
        String[] s = new String[value.length / 2]; //3
        for (int i = 0; i < value.length; i += 2) {
            s[i / 2] = value[i] + "," + value[i + 1];
        }
        return s;
    }
}
